import "./style.scss";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import {
  Col,
  Row,
  Select,
  BackTop,
  Input,
  Button,
  Modal,
  Checkbox,
  Upload,
  Switch,
  Form,
  message,
  notification,
} from "antd";
import React, { useEffect, useState } from "react";
import "katex/dist/katex.css";
import socketIOClient from "socket.io-client";
import moment from "moment";
import apiUser from "./src/api/user";
import api from "../../api/post";
import Comfirm from "../comfirm";
import { getSubject } from "../../utils/champion-user";
import { validate } from "@babel/types";
import axios from "axios";
const socket = socketIOClient(process.env.REACT_APP_API_SOCKET_IO);
const { Option } = Select;

const AskTemplate = (props) => {
  const [image, setImage] = useState("");
  const { isModalAsk, setIsModalAsk } = props;
  const [content, setContent] = useState("");
  const [fileList, setFileList] = useState("");
  const [previewVisible, setPreviewVisible] = useState(false);
  const [isComfirm, setIsComfirm] = useState(false);
  const [option, setOption] = useState([]);
  const [checked, setChecked] = useState(true);
  const [loading, setLoading] = useState(false);
  const dataSubject = async () => {
    const data = await api.getSubject();
    const listSubject = data?.map((list) => {
      return { value: list.id, label: list.name };
    });
    setOption(listSubject);
  };

  const handleChange = () => {
    setIsModalAsk(false);
  };

  const handlePreview = (file) => {
    console.log("file", file);
    setPreviewVisible(true);
    setFileList(file.url || file.thumbUrl);
  };
  const uploadImage = async (e) => {
    // const files = e.target.files
    console.log(e.file.originFileObj);
    const data = new FormData();
    data.append("file", e.file.originFileObj);
    data.append("upload_preset", "thanhqn");
    setLoading(true);
    const res = await fetch(
      "https://api.cloudinary.com/v1_1/dca35wzo4/image/upload",
      {
        method: "POST",
        body: data,
      }
    );
    const file = await res.json();
    setImage(file.secure_url);
    setLoading(false);
  };
  const handleChangeImage = (files) => {
    console.log(files.fileList);
  };
  const handleCancelImage = () => {
    setPreviewVisible(false);
  };

  useEffect(() => {
    dataSubject();
  }, []);

  const contentPost = (content, delta, source, editor) => {
    setContent(editor.getText());
  };

  const postAnonymous = (event) => {
    if (event == false) {
      setIsComfirm(true);
    } else {
      setChecked(true);
    }
  };

  const onFinish = async (values) => {
    const id = JSON.parse(localStorage.getItem("Authorization1")).user;
    const user = await apiUser.getUserById(id.id);
    const usersPost = {
      id: user.id,
      username: user.fullname,
      fullname: user.fullname,
      email: user.email,
      point: user.point,
      role: user.role,
      create_at: user.fullname.createAt,
      image: user.image,
    };
    const datapost = {
      room: "home",
      id: 0,
      type: 1,
      anonymus: checked ? 1 : 0,
      display_status: checked ? 1 : 0,
      question: {
        id: 1,
        content: content,
        point: Number(values.point),
        img: null,
        title: values.title,
        updateDay: moment().format("DD-MM-YYYY hh:mm:ss"),
        createDay: moment().format("DD-MM-YYYY hh:mm:ss"),
      },
      tag: values.subject.map((data) => {
        return { id: Number(data), name: getSubject(Number(data)) };
      }),
      comment: 0,
      like: 0,
      usersPost: usersPost,
      disLike: 0,
      detailedComment: 0,
    };
    setLoading(true);
    socket.emit("NewBlog", datapost);
    setLoading(false);
    setIsModalAsk(false);
    message.info("Câu hỏi của bạn đã được tải lên thành công!");
  };
  return (
    <>
      <Modal
        centered
        className="details-modal"
        visible={isModalAsk}
        footer={null}
        width={800}
        closable={true}
        onCancel={handleChange}
        title="Hãy đặt ra câu hỏi của bạn"
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Row className="content">
            <Col className="selection" span={24}>
              <Form.Item
                name="subject"
                rules={[{ required: true, message: "Subject is required" }]}
              >
                <Select
                  mode="multiple"
                  placeholder="Please select"
                  className="selection-op"
                >
                  {option.map((data, key) => {
                    return (
                      <Option key={key} value={data.value}>
                        {data.label}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
              <Form.Item
                name="point"
                label="Point"
                rules={[{ required: true, message: "Point is required" }]}
              >
                <Select defaultValue="" className="selection-option">
                  <Option value="10">10</Option>
                  <Option value="20">20</Option>
                  <Option value="30">30</Option>
                  <Option value="40">40</Option>
                  <Option value="50">50</Option>
                </Select>
              </Form.Item>
              <div>
                <Switch
                  checkedChildren="Công khai"
                  unCheckedChildren="Ẩn danh"
                  checked={checked}
                  defaultChecked
                  onChange={postAnonymous}
                />
              </div>
            </Col>
            <Col class="title-content" span={24}>
              <Form.Item
                name="title"
                rules={[
                  { required: true, message: "Title is required" },
                  { type: "string", max: 30 },
                ]}
              >
                <Input placeholder="Nhập tiêu đề của câu hỏi" />
              </Form.Item>
            </Col>
            <Col className="content-post-detail">
              <ReactQuill
                theme="snow"
                onChange={contentPost}
                placeholder="Controlled autosize"
              />
            </Col>
            <Col className="upload-file" span={24}>
              <div>
                <Upload
                  // action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                  listType="picture-card"
                  // fileList={fileList}
                  onPreview={handlePreview}
                  onChange={uploadImage}
                >
                  <div>
                    <i class="bx bx-upload"></i>
                  </div>
                </Upload>
                <h1>Upload Image</h1>
                <input
                  type="file"
                  name="file"
                  placeholder="Upload an image"
                  onChange={uploadImage}
                />
                <Modal
                  visible={previewVisible}
                  footer={null}
                  onCancel={handleCancelImage}
                >
                  <img alt="example" style={{ width: "100%" }} src={fileList} />
                </Modal>
              </div>
            </Col>
            <Col className="submit-post" span={24}>
              <div>Bạn hiện có 50 điểm</div>
              <div>
                <Button type="primary" htmlType="submit" loading={loading}>
                  {loading ? "Loading..." : "Gửi câu hỏi"}
                </Button>
              </div>
            </Col>
          </Row>
        </Form>
      </Modal>
      <Comfirm
        isComfirm={isComfirm}
        setIsComfirm={setIsComfirm}
        setChecked={setChecked}
      />
    </>
  );
};

export default AskTemplate;
