import HomeTemplate from "../components/home";
import BlogTemplate from "../components/blog";
import PostUser from "../components/templates/post/post-user";
import UserInforDetail from "../components/templates/user/user-infor-detail";
import EditPorfileUser from "../components/templates/user/user-edit";
import RankInfor from "../pages/rank";
import LoginGG from "../pages/login/index";
import BlogDetails from "../components/templates/blog-detail";
import Guide from "../components/linkinPage/guide";
import Policy from "../components/linkinPage/policy";
import Rule from "../components/linkinPage/rule";

const routes = [
  {
    path: "/",
    component: HomeTemplate,
  },
  {
    path: "/blog",
    component: BlogTemplate,
  },
  {
    path: "/post-detail/:id",
    component: PostUser,
  },
  {
    path: "/user/profile",
    component: UserInforDetail,
  },
  {
    path: "/user/edit",
    component: EditPorfileUser,
  },
  {
    path: "/rank-fpoly",
    component: RankInfor,
  },
  {
    path: "/blog-detail/:id",
    component: BlogDetails,
  },
  {
    path: "/huong-dan",
    component: Guide,
  },
  {
    path: "/dieu-khoan",
    component: Policy,
  },
  {
    path: "/noi-quy",
    component: Rule,
  },
];

export default routes;
