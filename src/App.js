import "antd/dist/antd.css";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { rootReducer } from "./store/reducers/rootReducer";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import LayoutTemplate from "./pages/layout";
import LoginGG from "./pages/login";
import { useEffect, useState } from "react";

const App = () => {
  const [router, setRouter] = useState("");
  const store = createStore(rootReducer, applyMiddleware(thunk));
  const dataUser = JSON.parse(localStorage.getItem("Authorization1"));
  const getRoom = window.location.href.split("/");
  console.log("rooom", getRoom[getRoom.length - 1]);
  useEffect(() => {
    setRouter(getRoom[getRoom.length - 1]);
  }, [getRoom]);
  return (
    <Provider store={store}>
      {router !== "login" ? (
        <LayoutTemplate />
      ) : (
        <Router>
          <Route path="/login" component={LoginGG} />
        </Router>
      )}
    </Provider>
  );
};

export default App;
