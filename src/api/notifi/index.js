import { customAxios } from "../../utils/custom-axios";
const serverEndpoint = process.env.REACT_APP_API_URL;

export const handleResponse = (res) => {
  const data = res?.data?.data;
  return data;
};

export const handleError = (err) => {
  console.error(err);
  throw err;
};

export default {
  getNotifiById: async (id) => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/notification/get/${id}`,
    });
    return res.data;
  },
  getNotifiAll: async () => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/notification/get`,
    });
    return res.data;
  },
  updateNotiById: async (id) => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/notification/update/${id}`,
    });
    return res.data;
  },
};