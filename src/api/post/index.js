import { customAxios } from "../../utils/custom-axios";
const serverEndpoint = process.env.REACT_APP_API_URL;

export const handleResponse = (res) => {
  const data = res?.data?.data;
  return data;
};

export const handleError = (err) => {
  console.error(err);
  throw err;
};

export default {
  //Post
  getPostAll: async () => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/post/getAll`,
    });
    return res.data;
  },

  getPostById: async (id) => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/post/getById/${id}`,
    });
    return res.data;
  },

  updatePostById: async (data, id) => {
    const response = await customAxios({
      method: "put",
      data: data,
      url: `${serverEndpoint}/post/put/${id}`,
    });
    return response.data;
  },

  deletePostById: async (id) => {
    return customAxios({
      method: "delete",
      url: `${serverEndpoint}/post/delete/${id}`,
    })
      .then((res) => res.data)
      .catch((err) => {
        throw err;
      });
  },

  getSubject: async () => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/subject/get`,
    });
    return res.data;
  },
};
