import { customAxios } from "../../utils/custom-axios";

const serverEndpoint = process.env.REACT_APP_API_URL;


export const handleResponse = (res) => {
    const data = res?.data?.data;
    return data;
};

export const handleError = (err) => {
    console.error(err);
    throw err;
};

export default {
  
  //User
  getUserAll: async () => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/user/get`,
    });

    return res.data;
  },

  getUserById: async (id) => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/user/get/${id}`,
    });
    return res.data;
  },

  updateUsersById: async (id,data) => {
    const response = await customAxios({
      method: "put",
      data: data,
      url: `${serverEndpoint}/user/put/${id}`,
    });
    return response.data;
  },

  
  //Reply
  getDataReply: async () => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/reply/get`,
    });

    return res.data;
  },

  createReply: async (data) => {
    return customAxios({
      method: "post",
      data: data,
      url: `${serverEndpoint}/reply/post`,
    })
      .then((res) => res.data)
      .catch((err) => {
          throw err;
      });
  },

  updateReplyById: async (data, id) => {
    const response = await customAxios({
      method: "put",
      data: data,
      url: `${serverEndpoint}/reply/put/${id}`,
    });
    return response.data;    
  },

  deleteReplyById: async (id) => {
    return customAxios({
      method: "delete",
      url: `${serverEndpoint}/reply/delete/${id}`,
    })
      .then((res) => res.data)
      .catch((err) => {
          throw err;
      });
  },

  updateUserProfile: async (id, data) => {
    const response = await customAxios({
      method: "put",
      data: data,
      url: `${serverEndpoint}/user/putProfile/${id}`,
    });
    return response.data;    
  },
  
};


