import { customAxios } from "../../utils/custom-axios";
const serverEndpoint = process.env.REACT_APP_API_URL;

export const handleResponse = (res) => {
  const data = res?.data?.data;
  return data;
};

export const handleError = (err) => {
  console.error(err);
  throw err;
};

export default {
  //ques
  getsSatistical: async () => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/statistical/get`,
    });
    return res.data;
  },

  reportQuestion: async (param) => {
    const res = await customAxios({
      method: "post",
      data: param,
      url: `${serverEndpoint}/report/question`,
    });
    return res.data;
  },

  reportReply: async (id) => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/statistical/gettime/${id}`,
    });
    return res.data;
  },

  reportComment: async (param) => {
    const res = await customAxios({
      method: "post",
      data: param,
      url: `${serverEndpoint}/report/reply`,
    });
    return res.data;
  },

  getMyInteractive: async (id) => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/user/getProfile/${id}`,
    });
    return res.data;
  },
};
