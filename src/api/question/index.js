import { customAxios } from "../../utils/custom-axios";
const serverEndpoint = process.env.REACT_APP_API_URL;

export const handleResponse = (res) => {
  const data = res?.data?.data;
  return data;
};

export const handleError = (err) => {
  console.error(err);
  throw err;
};

export default {
  //ques
  getQuestionAll: async () => {
    const res = await customAxios({
      method: "get",
      url: `${serverEndpoint}/question/getAll`,
    });
    return res.data;
  },

  getQuestionById: async (param) => {
    const res = await customAxios({
      method: "post",
      data:param,
      url: `${serverEndpoint}/question/postAll`,
    });
    return res.data;
  },

  updateQuestionById: async (data, id) => {
    const response = await customAxios({
      method: "put",
      data: data,
      url: `${serverEndpoint}/question/put/${id}`,
    });
    return response.data;
  },

  deleteQuestionById: async (id) => {
    return customAxios({
      method: "delete",
      url: `${serverEndpoint}/question/delete/${id}`,
    })
      .then((res) => res.data)
      .catch((err) => {
        throw err;
      });
  },
  feelQuestion: async (param) => {
    const res = await customAxios({
      method: "post",
      data:param,
      url: `${serverEndpoint}/vote/action`,
    });
    return res.data;
  },
  getQuestionUpdate: async (param) => {
    const res = await customAxios({
      method: "post",
      data:param,
      url: `${serverEndpoint}/question/post`,
    });
    return res.data;
  },
};
