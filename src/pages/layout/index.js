import "./style.scss";
import HeaderTemplate from "./header/index";
import FooterTemplate from "./footer/index";
import { Affix, BackTop, Button } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import AskTemplate from "../../components/ask";
import { useState } from "react";
import LoginGG from "../login";
import { useEffect } from "react";

const LayoutTemplate = () => {
  const [isModalAsk, setIsModalAsk] = useState(false);
  const dataUser = JSON.parse(localStorage.getItem("Authorization1"));
  const showModal = () => {
    setIsModalAsk(true);
  };

  return (
    <>
      {dataUser ? (
        <>
          <div className="background">
            <HeaderTemplate />
            <FooterTemplate />
            <BackTop>
              <div>
                <i class="bx bxs-chevron-up"></i>
              </div>
            </BackTop>
            <Affix className="affix-button">
              <div style={{ display: "flex", alignItems: "center" }}>
                <div className="rectangle">Đặt câu hỏi</div>
                <div class="arrow-right"></div>
                <Button
                  onClick={showModal}
                  type="primary"
                  icon={<PlusOutlined style={{ fontSize: "40px" }} />}
                ></Button>
              </div>
            </Affix>
          </div>
        </>
      ) : (
        <LoginGG />
      )}
      <AskTemplate isModalAsk={isModalAsk} setIsModalAsk={setIsModalAsk} />
    </>
  );
};

export default LayoutTemplate;
