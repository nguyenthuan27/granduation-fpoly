import { Col, Row } from "antd";
import "./styles.scss";

const FooterTemplate = () => {
  return (
    <Row className="footer">
      <Col span={24} className="footer-content">
        <Row className="content-details">
          <Col span={6} className="detail-inf">
            <div className="logo">
              <img src="./images/fpt.png" />
            </div>
            <div className="title">
              <span>Ứng dụng hỏi đáp dành cho sinh viên Fpoly</span>
            </div>
            <div className="social-network">
              <i class="bx bxl-facebook-square"></i>
              <i class="bx bxl-youtube"></i>
            </div>
          </Col>
          <Col span={6} className="action">
            <ul className="action-list">
              <li>
                <a href="/huong-dan">Hướng dẫn sử dụng</a>
              </li>
              <li>
                <a href="/dieu-khoan">Điều khoản sử dụng</a>
              </li>
              <li>
                <a href="/noi-quy">Nội quy Fpoly Q&A</a>
              </li>
            </ul>
          </Col>
          
        </Row>
      </Col>
      <Col span={24} className="copyright">
        © 2021 Nhóm sinh viên free fire team fpoly
      </Col>
    </Row>
  );
};

export default FooterTemplate;
