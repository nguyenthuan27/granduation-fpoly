import "./style.scss";
import { Button, Col, Input, Row, Menu } from "antd";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import routes from "../../../router";
import { useHistory } from "react-router-dom";
import UserClick from "../../../components/templates/user/user-click";
import { useState } from "react";
import LoginGG from "../../login";
const HeaderTemplate = () => {
  const [valueFindter, setValueFindter] = useState("");
  const dataUser = JSON.parse(localStorage.getItem("Authorization1"));
  const routeComponents = routes.map(({ path, component }, key) => (
    <Route exact path={path} component={component} key={key} />
  ));
  const inputValue = (e) => {
    setValueFindter(e.target.value);
  };
  const Sub = () => {
    // let history = useHistory();
    // history.push({
    //   path: '/',
    //   state: {d: valueFindter }
    // })
  };

  return (
    <Router>
      <div className="header">
        <Row className="header-menu">
          <Col span={3} className="logo-header">
            <Link to="/">
              {" "}
              <img src="../images/logo.png" />
            </Link>
          </Col>
          <Col span={3} className="menu">
            <Menu className="menu-custom">
              <Menu.Item key="1">
                <Link to={{ pathname: "/", state: true }}>Home</Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/blog">Blog</Link>
              </Menu.Item>
            </Menu>
          </Col>
          <Col span={6} className="header-search">
            {/* <Input
              placeholder="input search text"
              maxLength={25}
              onChange={inputValue}
            />
            <Button onClick={Sub}>Tìm kiếm</Button> */}
          </Col>
          <Col span={4} className="menu-user">
            <div className="user-inf">
              {/* <div className="user-login">Login</div>
              <div className="user-register">Register</div> */}
              <UserClick />
            </div>
          </Col>
          <Col span={0}></Col>
        </Row>
      </div>
      <Switch>{routeComponents}</Switch>
    </Router>
  );
};

export default HeaderTemplate;
