import "./style.scss";
import { Button, Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
import branchApi from '../../api/branch/index';
import loginApi from '../../api/login/index';
import Login from '../../pages/login/Login';
import {GoogleLogin} from 'react-google-login';

const { Option } = Select;

const LoginGG = () => {
  const [listBranch, setListBranch] = useState([])
  const [branch, setBranch] = useState("Lựa chọn cơ sở");
  const handleChange = (value) => {
    console.log(branch)
    setBranch(value);
  };
  const onSuccess = (res) =>{
    console.log(res.profileObj);
};
const onFailure = (res) =>{
    console.log("Login failed", res);
};

  useEffect(() => {
    const fetchBranchList = async () => {
      try {
        const response = await branchApi.getBranchAll();
        setListBranch(response)
      } catch (error) {
      }
    }
    fetchBranchList();
  }, [])

  return (
    <div className="login">
      <Row className="login-content">
        <Col span={24} className="logo">
          <img src="./images/logo.png" />
        </Col>
        <Col span={24} className="euclid">
          <Select defaultValue="Lựa chọn cơ sở" style={{ width: 350 }} value={branch} onChange={handleChange}>
            {listBranch.map((data, key) => {
              return (
                <Option key={key} value={data.id}>
                  {data.name}
                </Option>
              );
            })}
          </Select>
        </Col>


        <Col span={24} className="google">
          {branch !== "Lựa chọn cơ sở" ?
            <Login branches={branch} />
            :
            <GoogleLogin
            disabled={true}
            clientId='439536812760-j1a0qvadh5ea4rkf9qjdcnh6nudiqpkm.apps.googleusercontent.com'
            buttonText="Login with Google"
            onSuccess={onSuccess}
            onFailure={onFailure}
            cookiePolicy={'single_host_origin'}
            isSignedIn={true}
            />
          }
        </Col>
      </Row>
    </div>
  );
};
export default LoginGG;
