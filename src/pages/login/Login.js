import React from "react";
import { GoogleLogin } from "react-google-login";
import { useEffect, useState } from "react";
import loginApi from "../../api/login/index";
import { message } from "antd";

const clientId =
  "439536812760-j1a0qvadh5ea4rkf9qjdcnh6nudiqpkm.apps.googleusercontent.com";

const Login = (props) => {
  const [isSignedIn, setIsSignedIn] = useState(false);
  const onSuccess = async (res) => {
    console.log("userr Poly", res);
    const data = {
      email: res.profileObj.email,
      key: res.profileObj.googleId,
      cs: props.branches,
      username: res.profileObj.name,
      img: res.profileObj.imageUrl,
    };
    console.log("data", data);
    const login = await loginApi.postLogin(data);
    console.log("login", login);
    if (login) {
      setIsSignedIn(true);
      window.localStorage.setItem("Authorization1", JSON.stringify(login));
      window.location.reload();
      window.location.href = "/";
    } else {
      message.error("Login failed");
    }
  };
  const onFailure = (res) => {
    setIsSignedIn(false);
    console.log("Login failed", res);
  };

  return (
    <div>
      {isSignedIn !== true ? (
        <GoogleLogin
          clientId={clientId}
          buttonText="Login with Google"
          onSuccess={onSuccess}
          onFailure={onFailure}
          cookiePolicy={"single_host_origin"}
          isSignedIn={true}
        />
      ) : (
        <GoogleLogin
          disabled={true}
          clientId={clientId}
          buttonText="Login with Google"
          onSuccess={onSuccess}
          onFailure={onFailure}
          cookiePolicy={"single_host_origin"}
          isSignedIn={true}
        />
      )}
    </div>
  );
};

export default Login;
