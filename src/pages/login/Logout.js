import React from "react";
import { GoogleLogout } from "react-google-login";
import { useHistory } from "react-router-dom";
const clientId =
  "439536812760-j1a0qvadh5ea4rkf9qjdcnh6nudiqpkm.apps.googleusercontent.com";

const Logout = () => {
  const onSuccess = () => {
    window.localStorage.removeItem("Authorization1");
    window.location.reload();
    window.location.href = "/login";
  };

  return (
    <div className="logout">
      <GoogleLogout
        clientId={clientId}
        buttonText="Logout"
        onLogoutSuccess={onSuccess}
      />
    </div>
  );
};

export default Logout;
