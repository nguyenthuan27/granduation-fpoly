import "./style.scss";
import { Button, Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
import UserContent from "../../components/templates/user/user-content";
import PostList from "../../components/post";
import RankItem from "./rankItem";
import { listRank } from "../../utils/champion-user";
const { Option } = Select;
const RankInfor = () => {
  return (
    <div className="rank-content">
      <Row className="rank">
        <Col span={16} className="list-rank">
          {listRank.map((rank) => (
            <RankItem {...rank} key={rank.id} />
          ))}
        </Col>
        <Col span={8}>
          <UserContent />
        </Col>
      </Row>
    </div>
  );
};
export default RankInfor;
