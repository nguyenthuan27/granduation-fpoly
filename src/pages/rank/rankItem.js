import "./rank.scss";
import { Button, Col, Row, Select } from "antd";
import { useEffect, useState } from "react";
const { Option } = Select;

const RankItem = (props) => {
  const { title, point, content, img } = props;
  return (
    <div className="rank-item">
      <Row className="item">
        <Col span={5} className="image">
          <img src={img} />
        </Col>
        <Col span={19} className="content">
          <div className="title">
            <b>{title}</b>
          </div>
          <div className="point">
            <span>{point} điểm</span>
          </div>
          <div className="inf">
            <span>{content}</span>
          </div>
        </Col>
      </Row>
    </div>
  );
};
export default RankItem;
