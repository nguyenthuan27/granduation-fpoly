import "./style.scss";
import { Col, Row, Menu, Select, BackTop, Checkbox, Button, Affix } from "antd";
import SortPost from "../templates/post/sort-post";
import { PlusOutlined, wechat, WechatOutlined } from '@ant-design/icons';
import UserContent from "../templates/user/user-content";
import PostList from "../post";
import { useEffect, useState } from "react";
import SortSubject from "../templates/post/sort-subject";
import { useLocation } from 'react-router-dom';
const Guide = (porps) => {
  const [subjectSortData, setSubjectSortData] = useState([]);
  const [dataSort, setDataSort] = useState("");
  const [dataFind, setDataFind] = useState("");
  const location = useLocation();
  useEffect(() => {
    console.log(location.pathname); // result: '/secondpage'
    console.log(location.state); // result: 'some_value'
 }, [location]);
 console.log(porps.location.state)
  return (
    <div className="home">
      <Row className="home-content">
        <Col span={4} className="menu-custom">
          
        </Col>
        <Col span={14} className="home-posts">
        <h1 class="big-title">HƯỚNG DẪN SỬ DỤNG TRANG WEB FPOLY Q&A</h1>
          <Row className="guid-list">
            <div>
            <h3><a class="cl-blue" ><span class="guide-number bg-blue cl-f"></span>Hướng dẫn gửi câu hỏi</a></h3>
            <p><span>1. Bấm vào nút đặt câu hỏi.</span></p>
            <p><span>2. Chọn chế độ đặt câu hỏi: Công khai hoặc ẩn danh.</span></p>
            <p><span>3. Điền thông tin câu hỏi của bạn gồm: Ngôn ngữ, số điểm của câu hỏi, tiêu đề, nội dung(ảnh) của câu hỏi.</span></p>
            <p><span>4. Gửi câu hỏi để có thể nhận được câu trả lời sớm nhất.</span></p>
            </div>
            <div>
            <h3><a class="cl-blue" ><span class="guide-number bg-blue cl-f"></span>Hướng dẫn trả lời câu hỏi</a></h3>
            <p><span>1. Chọn câu hỏi bạn muốn trả lời.</span></p>
            <p><span>2. Bấm nút trả lời ngay.</span></p>
            <p><span>3. Điền thông tin câu trả lời của bạn: nội dung(ảnh) của câu trả lời.</span></p>
            <p><span>4. Gửi câu trả lời của bạn để có thể nhận được điểm thưởng.</span></p>
            </div>
            <div>
            <h3><a class="cl-blue" ><span class="guide-number bg-blue cl-f"></span>Hướng dẫn cộng điểm</a></h3>
            <p><span>1. Bạn sẽ bị trừ số điểm tương đương số điểm bạn chọn khi đặt ra mỗi câu hỏi.</span></p>
            <p><span>2. Với mỗi câu trả lời hơp lệ bạn sẽ nhận được 50% số điểm của câu hỏi.</span></p>
            <p><span>3. Với mỗi báo cáo hơp lệ bạn sẽ nhận được 5 điểm và 10% số điểm của câu hỏi(câu trả lời) mà bạn báo cáo.</span></p>
            <p><span>4. Với mỗi báo cáo không hơp lệ bạn sẽ bị trừ 10 điểm.</span></p>
            </div>
          </Row>
        </Col>
        <Col span={6}>
          <UserContent />
        </Col>
      </Row>
    </div>
  );
};

export default Guide;
