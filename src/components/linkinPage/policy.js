import "./style.scss";
import { Col, Row, Menu, Select, BackTop, Checkbox, Button, Affix } from "antd";
import SortPost from "../templates/post/sort-post";
import { PlusOutlined, wechat, WechatOutlined } from '@ant-design/icons';
import UserContent from "../templates/user/user-content";
import PostList from "../post";
import { useEffect, useState } from "react";
import SortSubject from "../templates/post/sort-subject";
import { useLocation } from 'react-router-dom';
const Policy = (porps) => {
  const [subjectSortData, setSubjectSortData] = useState([]);
  const [dataSort, setDataSort] = useState("");
  const [dataFind, setDataFind] = useState("");
  const location = useLocation();
  useEffect(() => {
    console.log(location.pathname); // result: '/secondpage'
    console.log(location.state); // result: 'some_value'
 }, [location]);
 console.log(porps.location.state)
  return (
    <div className="home">
      <Row className="home-content">
        <Col span={4} className="menu-custom">
          
        </Col>
        <Col span={14} className="home-posts">
        <h1 class="big-title">ĐIỀU KHOẢN SỬ DỤNG</h1>
          <Row className="guid-list">
          <div>
            <p><strong>Chào mừng bạn đến với Fpoly Q&A!</strong></p>
            <p><span>Fpoly Q&A là một cộng đồng trao đổi, chia sẻ kiến thức tại trường cao đẳng FPT Polytechnic. Fpoly Q&A khuyến khích tất cả mọi người, đặc biệt là các bạn học sinh chia sẻ và khám phá kiến thức trong một không gian cởi mở, thân thiện, đồng thời thúc đẩy, phát triển mô hình giáo dục “đồng hành” thay cho mô hình “truyền đạt” truyền thống.</span></p>
          
            </div>
          </Row>
        </Col>
        <Col span={6}>
          <UserContent />
        </Col>
      </Row>
    </div>
  );
};

export default Policy;
