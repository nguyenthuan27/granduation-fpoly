import "./style.scss";
import { Col, Row, Menu, Select, BackTop, Checkbox, Button, Affix } from "antd";
import SortPost from "../templates/post/sort-post";
import { PlusOutlined, wechat, WechatOutlined } from '@ant-design/icons';
import UserContent from "../templates/user/user-content";
import PostList from "../post";
import { useEffect, useState } from "react";
import SortSubject from "../templates/post/sort-subject";
import { useLocation } from 'react-router-dom';
const Rule = (porps) => {
  const [subjectSortData, setSubjectSortData] = useState([]);
  const [dataSort, setDataSort] = useState("");
  const [dataFind, setDataFind] = useState("");
  const location = useLocation();
  useEffect(() => {
    console.log(location.pathname); // result: '/secondpage'
    console.log(location.state); // result: 'some_value'
 }, [location]);
 console.log(porps.location.state)
  return (
    <div className="home">
      <Row className="home-content">
        <Col span={4} className="menu-custom">
          
        </Col>
        <Col span={14} className="home-posts">
        <h1 class="big-title">NỘI QUY</h1>
          <Row className="guid-list">
          <div>
            <p><strong>Chào mừng bạn đến với Fpoly Q&A!</strong></p>
            <p><strong>1.</strong> Luôn giải thích đầy đủ các bước giải, đáp án là theo cách đầy đủ nhất.</p>
            <p><strong>2.</strong> Không spam, trả lời linh tinh để tăng điểm, vi phạm sẽ bị khoá tài khoản.</p>
            <p><strong>3.</strong> Không sao chép mạng vì người hỏi cần câu trả lời của riêng bạn.</p>
            <p ><strong>4.</strong> Không tạo nhiều tài khoản để tăng điểm gian lận, vi phạm sẽ bị khóa tài khoản từ 7 ngày đến vĩnh viễn.</p>
            <p ><strong>5.</strong> Không chia sẻ thông tin cá nhân tránh bị làm phiền hoặc làm phiền người khác.</p>
            <p ><strong>6.</strong> Không đặt tên tài khoản, avatar gây hiểu nhầm là thành viên ban quản trị Fpoly Q&A hoặc có chưa từ ngữ nhạy cảm.</p>
            <p ><strong>7.</strong> Không đăng tải nội dung nhạy cảm, thiếu chính xác, gây mâu thuẫn và hiềm khích, vi phạm sẽ bị khoá tài khoản từ 7 ngày đến vĩnh viễn.</p>
            <p ><strong>8.</strong> Tôn trọng thành viên khác, không sử dụng từ ngữ thô tục, phản cảm, xúc phạm thành viên, luôn tuân thủ quy định luật pháp Việt Nam trên môi trường mạng.</p>
            </div>
          </Row>
        </Col>
        <Col span={6}>
          <UserContent />
        </Col>
      </Row>
    </div>
  );
};

export default Rule;
