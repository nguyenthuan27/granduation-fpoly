import "./style.scss";
import { Col, Row, Menu, Select, BackTop, Empty, Pagination, Spin } from "antd";
import postApi from "../../api/post";
import { useEffect, useState } from "react";
import PostContent from "../templates/post/post-content";
import socketIOClient from "socket.io-client";
import _ from "lodash";
const socket = socketIOClient(process.env.REACT_APP_API_SOCKET_IO);


const PostList = (props) => {
  const { subjectSortData, dataSort} = props;
  console.log("form load",dataSort)
  const room = "home";
  socket.emit("joinroom", room);
  const [currentShowingPost, setCurrentShowingPost] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [sourcePost, setSourcePost] = useState([]);
  const [loading, setLoading] = useState(false);
  const [filteredPost, setFilteredPost] = useState([]);
  const [filteredPostV2, setFilteredPostV2] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [countPost, setCountPost] = useState(0);
  const [newsubjectSortData,setNewsubjectSortData] =useState([]);
  const getAllPost = async () => {
    setLoading(true);
    let posts = await postApi.getPostAll();
    setSourcePost(posts);
    setFilteredPost(posts);
    setCurrentShowingPost(
      [...filteredPost].splice((currentPage - 1) * pageSize, pageSize)
    );
    setLoading(false);
  };

  const handlePageChange = (currentPage) => {
    setCurrentPage(currentPage);
    setCurrentShowingPost(
      [...filteredPost].splice((currentPage - 1) * pageSize, pageSize)
    );
  };
  const onChangeSort = () => {
    if (dataSort == "all") {
      setFilteredPost(sourcePost);
    } else if (dataSort == "post-asc") {
      const reordered = _(filteredPost).orderBy(
        (load) => new Date(load?.question?.createDay),
        "desc"
      );
      setFilteredPost(reordered);
    } else if (dataSort == "post-dsc") {
      const reordered = _(filteredPost).orderBy(
        (load) => new Date(load?.question?.createDay),
        "asc"
      );
      setFilteredPost(reordered);
    } else if (dataSort == "answe") {
      setFilteredPostV2(filteredPost)
      const reordered = _.filter( filteredPostV2.length==0?filteredPost:filteredPostV2, (data) => {
        return data?.comment >= 1;
      });
      setFilteredPost(reordered);
    } else if (dataSort == "notanswered") {
      setFilteredPostV2(filteredPost)
      const reordered = _.filter(filteredPostV2.length==0?filteredPost:filteredPostV2, (data) => {
        return data?.comment == 0;
      });
   
      setFilteredPost(reordered);
    }
    setCurrentShowingPost(
      [...filteredPost].splice((currentPage - 1) * pageSize, pageSize)
    );
  };

  useEffect(() => {
    onChangeSort();
  }, [dataSort]);

  useEffect(() => {
    const dataFilter = [...sourcePost].filter((post) => {
      if (!subjectSortData.classes || !subjectSortData.classes.length)
        return true;
      const tag = post.tag.map((data) => {
        return data.id.toString();
      });
      return subjectSortData.classes.every((elem) => tag.includes(elem));
    });
    setFilteredPost(dataFilter);
  }, [subjectSortData]);

  useEffect(() => {
    setCurrentShowingPost(
      [...filteredPost].splice((currentPage - 1) * pageSize, pageSize)
    );
  }, [filteredPost]);

  useEffect(() => {
    getAllPost();
  }, []);
  useEffect(() => {
    socket.on("msgNewBlog", (data) => {
      console.log("hhh", data);
       setNewsubjectSortData(searches => [...searches, data])    
    });
    socket.on("msgErrormsgNewBlog", (data) => {
    });
    return () => {
      socket.destroy();
    };
  }, [socket]);
  useEffect(() => {
    if(newsubjectSortData.length!=0){
      setCountPost(newsubjectSortData.length)
      console.log(newsubjectSortData)
    }
    else{
      setCountPost(0)
    }
  }, [newsubjectSortData]);
  
  const show =()=>{
    setCountPost(0);
       newsubjectSortData.map((value)=>{
         console.log(value)
        setFilteredPost(searches => [value,...searches])
    })
   setNewsubjectSortData([])
   console.log(filteredPost[0])
  };
  console.log(filteredPost[filteredPost.length-1])
  return (
    <>
     {countPost === 0 ? (
              ""
            ) : (
              <Col span={24} className="total-new" onClick={show} >
                + {countPost} câu hỏi mới nhất
              </Col>
            )}
      {loading ? (
        <Spin size="large"></Spin>
      ) : (
        <div className="content-post">
          <Row className="list-post">
            {currentShowingPost.length ? (
              currentShowingPost.map((post) => (
                <PostContent {...post} key={post.id}></PostContent>
              ))
            ) : (
              <Empty
                className="empty"
                description="There are currently no posts"
                image={null}
                style={{ margin: "0px auto", color: "white", fontSize: "50px" }}
              ></Empty>
            )}
          </Row>
          <Row>
            <Pagination
              simple
              defaultCurrent={1}
              current={currentPage}
              pageSize={pageSize}
              total={filteredPost.length}
              onChange={handlePageChange}
            />
          </Row>
        </div>
      )}
    </>
  );
};

export default PostList;
