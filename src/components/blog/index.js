import "./style.scss";
import { Button, Col, Empty, Pagination, Row, Spin } from "antd";
import { useEffect, useState } from "react";
import blogApi from "../../api/blog";
import BlogItem from "../templates/blog-item";
import postApi from "../../api/post";
import { useHistory } from "react-router";
import PostBlog from "../templates/blog-modal";

const BlogTemplate = () => {
  const [isModalAsk, setIsModalAsk] = useState(false);
  const [currentBlogs, setCurrentBlogs] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentShowingPost, setCurrentShowingPost] = useState([]);
  const [filteredBlog, setFilteredBlog] = useState([]);
  const [loading, setLoading] = useState(false);
  const [pageSize, setPageSize] = useState(8);
  const [isLoading, setIsLoading] = useState(false);
  const history = useHistory();

  const getBlogAll = async () => {
    setLoading(true);
    let blog = await blogApi.getBlog();
    setFilteredBlog(blog);
    setCurrentBlogs([...blog].splice((currentPage - 1) * pageSize, pageSize));
    setLoading(false);
  };
  const handlePageChange = (currentPage) => {
    setCurrentPage(currentPage);
    setCurrentBlogs(
      [...filteredBlog].splice((currentPage - 1) * pageSize, pageSize)
    );
  };
  useEffect(() => {
    getBlogAll();
  }, [isModalAsk]);

  const getPost = async () => {
    setIsLoading(true);
    const post = await postApi.getPostAll();
    console.log("post", post);
    setIsLoading(false);
    setCurrentShowingPost(post.slice(0, 6));
  };

  const onPostDetail = (id) => {
    history.push("/post-detail/" + id);
  };
  useEffect(() => {
    getPost();
  }, []);
  const openModal = () => {
    setIsModalAsk(true);
  };
  return (
    <>
      <div className="blog">
        <Row className="blog-content">
          <Col span={16} className="blog-item">
            <Row className="content-blog">
              {loading ? (
                <Spin size="large"></Spin>
              ) : (
                <div>
                  {currentBlogs.length ? (
                    currentBlogs.map((blog) => (
                      <BlogItem {...blog} key={blog.id}></BlogItem>
                    ))
                  ) : (
                    <Empty
                      className="empty"
                      description="There are currently no posts"
                      image={null}
                      style={{
                        margin: "0px auto",
                        color: "white",
                        fontSize: "50px",
                      }}
                    ></Empty>
                  )}
                </div>
              )}
            </Row>
            <Row>
              <Pagination
                simple
                defaultCurrent={1}
                current={currentPage}
                pageSize={pageSize}
                total={filteredBlog.length}
                onChange={handlePageChange}
              />
            </Row>
          </Col>
          <Col span={8} className="blog-different">
            <Row>
              <Col span={24} className="question-new">
                <div className="selection-title">
                  <a>Câu hỏi mới nhất</a>
                  <div className="hr"></div>
                </div>
                <div className="content-different">
                  {loading ? (
                    <Spin size="large"></Spin>
                  ) : (
                    currentShowingPost.map((post) => (
                      <div className="post-item">
                        <div
                          className="title-post"
                          onClick={() => onPostDetail(post?.id)}
                        >
                          <span>{post?.question?.title}</span>
                        </div>
                        <div className="interactive">
                          <div style={{ marginRight: 10 }}>
                            <i class="bx bx-like"></i>
                            <span style={{ paddingLeft: 5 }}>{post?.like}</span>
                          </div>
                          <div>
                            <i class="bx bx-dislike"></i>
                            <span style={{ paddingLeft: 5 }}>
                              {post?.disLike}
                            </span>
                          </div>
                          <div style={{ marginLeft: 10 }}>
                            <i class="bx bx-chat"></i>
                            <span style={{ paddingLeft: 5 }}>
                              {post?.comment}
                            </span>
                          </div>
                        </div>
                        <div className="user">
                          <span>{post?.usersPost?.fullname}</span>
                        </div>
                      </div>
                    ))
                  )}
                </div>
              </Col>
              <Col span={24} className="start-ask">
                <div className="ask-title">Chia sẻ kiến thức</div>
                <Button onClick={() => openModal()}>ĐẶT CÂU HỎI</Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
      <PostBlog isModalAsk={isModalAsk} setIsModalAsk={setIsModalAsk} />
    </>
  );
};

export default BlogTemplate;
