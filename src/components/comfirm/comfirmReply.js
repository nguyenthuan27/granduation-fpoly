import { Col, message, Modal, Row } from "antd";
import reportApi from "../../api/report";

const ComfirmReportReply = (props) => {
  const { isCheck, setIsCheck, content, idReply, setIsReportReply } = props;
  const hideModal = () => {
    setIsCheck(false);
  };
  const userInf = JSON.parse(localStorage.getItem("Authorization1")).user;
  const comfirmOk = async () => {
    const param = {
      idusers: userInf.id,
      content: content,
      reply: idReply,
    };
    await reportApi.reportComment(param);
    message.success("Bạn đã báo cáo câu trả lời này thành công!");
    setIsReportReply(false);
    setIsCheck(false);
  };
  return (
    <>
      <Modal
        title="Thông báo"
        visible={isCheck}
        onOk={comfirmOk}
        onCancel={hideModal}
        okText="Ok"
        cancelText="Cancel"
      >
        <p>Bạn có chắc chắn muốn báo cáo câu trả lời này không?</p>
      </Modal>
    </>
  );
};

export default ComfirmReportReply;
