import { Col, message, Modal, Row } from "antd";
import reportApi from "../../api/report";

const Notifi = (props) => {
  const { isCheck, setIsCheck, content, questionId, setIsModalAsk } = props;
  const hideModal = () => {
    setIsCheck(false);
  };
  const userInf = JSON.parse(localStorage.getItem("Authorization1")).user;
  const comfirmOk = async () => {
    const param = {
      idusers: userInf.id,
      content: content,
      question: questionId,
    };
    await reportApi.reportQuestion(param);
    message.success("Bạn đã báo cáo bài viết thành công!");
    setIsModalAsk(false);
    setIsCheck(false);
  };
  return (
    <>
      <Modal
        title="Thông báo"
        visible={isCheck}
        onOk={comfirmOk}
        onCancel={hideModal}
        okText="Ok"
        cancelText="Cancel"
      >
        <p>Bạn có chắn chắn muốn báo cáo bài viết này không?</p>
      </Modal>
    </>
  );
};

export default Notifi;
