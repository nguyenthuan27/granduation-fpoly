import { Col, Modal, Row } from "antd";

const Comfirm = (props) => {
  const { isComfirm, setIsComfirm, setChecked } = props;
  const hideModal = () => {
    setIsComfirm(false);
  };
  const comfirmOk = () => {
    setChecked(false)
    setIsComfirm(false);
  }
  return (
    <>
      <Modal
        title="Thông báo"
        visible={isComfirm}
        onOk={comfirmOk}
        onCancel={hideModal}
        okText="Ok"
        cancelText="Cancel"
      >
        <p>Bạn có chắn chắn muốn để bài viết với chế độ ẩn danh không?</p>
        <p>Bài viết của ban sẽ được kiểm duyệt. Bạn có đồng ý không?</p>
      </Modal>
    </>
  );
};

export default Comfirm;
