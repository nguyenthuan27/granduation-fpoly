import "./style.scss";
import { Col, Row, Menu, Select, BackTop, Checkbox, Button, Affix } from "antd";
import SortPost from "../templates/post/sort-post";
import { PlusOutlined, wechat, WechatOutlined } from '@ant-design/icons';
import UserContent from "../templates/user/user-content";
import PostList from "../post";
import { useEffect, useState } from "react";
import SortSubject from "../templates/post/sort-subject";
import { useLocation } from 'react-router-dom';
const HomeTemplate = (porps) => {
  const [subjectSortData, setSubjectSortData] = useState([]);
  const [dataSort, setDataSort] = useState("");
  return (
    <div className="home">
      <Row className="home-content">
        <Col span={3} className="menu-custom">
          <SortSubject
            onChangeSubjectSortData={(sortData) => {
              setSubjectSortData(sortData);
            }}
          />
        </Col>
        <Col span={13} className="home-posts">
          <Row className="post-content">
            <Col span={24}>
              <SortPost dataSort={dataSort} setDataSort={setDataSort} />
            </Col>
            <Col span={24} className="post-item">
              <PostList
                dataSort={dataSort}
                subjectSortData={subjectSortData}
              />
            </Col>
          </Row>
        </Col>
        <Col span={8}>
          <UserContent />
        </Col>
      </Row>
    </div>
  );
};

export default HomeTemplate;
