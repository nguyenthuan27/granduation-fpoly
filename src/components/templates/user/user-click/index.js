import "./style.scss";
import { useHistory } from "react-router-dom";
import { Badge, Button, Col, Menu, message, Popover } from "antd";
import { BellOutlined } from "@ant-design/icons";
import UserMenu from "../menu-user";
import Notification from "../notification";
import { useEffect, useState } from "react";
import notifiApi from "../../../../api/notifi";
import useApi from "../../../../api/user";
const { SubMenu } = Menu;
const UserClick = () => {
  const history = useHistory();
  const [currentNotifi, setCurrentNotifi] = useState([]);
  const [noRead, setNoRead] = useState("");
  const [loading, setLoading] = useState(false);
  const [isLoad, setIsLoad] = useState("");
  const [userInfor, setUserInfor] = useState("");
  const onUserDetail = (id) => {
    history.push("/user/" + id);
  };
  const getNotifiAll = async () => {
    setLoading(true);
    const dataUser = JSON.parse(localStorage.getItem("Authorization1"))?.user;
    const notifi = await notifiApi.getNotifiById(dataUser?.id);
    setNoRead(notifi.filter((item) => item.type == false));
    setCurrentNotifi(notifi);
    setLoading(false);
  };

  useEffect(() => {
    (async () => {
      const userInf = JSON.parse(localStorage.getItem("Authorization1")).user;
      const user = await useApi.getUserById(userInf.id);
      console.log("user", user);
      setUserInfor(user);
    })();
  }, []);
  useEffect(() => {
    getNotifiAll();
  }, [isLoad]);
  return (
    <div className="user-event">
      <Popover
        placement="bottom"
        content={
          <Notification
            currentNotifi={currentNotifi}
            loading={loading}
            setIsLoad={setIsLoad}
          />
        }
        trigger="click"
        className="popover-content"
        span={12}
      >
        <Col span={3} className="notification">
          <Badge count={noRead.length}>
            <BellOutlined style={{ fontSize: 20 }} />
          </Badge>
        </Col>
      </Popover>
      <Popover
        placement="bottom"
        content={<UserMenu />}
        trigger="click"
        className="popover-content"
        span={10}
      >
        <Col span={3} className="user-img">
          <img src={userInfor.image} />
        </Col>
        <Col span={16} className="user-detail">
          <Popover content={userInfor?.fullname || ""}>
            {userInfor?.fullname || ""}
          </Popover>
        </Col>
      </Popover>
    </div>
  );
};

export default UserClick;
