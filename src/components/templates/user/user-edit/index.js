import {
  Avatar,
  Button,
  Col,
  Row,
  Select,
  Form,
  Input,
  DatePicker,
  Popover,
  Upload,
  message,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import "./style.scss";
import { useHistory } from "react-router-dom";
import { useEffect, useState } from "react";
import userApi from "../../../../api/user";
import branchApi from "../../../../api/branch";
import { getUserRank } from "../../../../utils/champion-user";
import Modal from "antd/lib/modal/Modal";
const { Option } = Select;

const EditPorfileUser = () => {
  const history = useHistory();
  const rank = () => {
    history.push("/rank-fpoly");
  };

  const [loading, setLoading] = useState(false);
  const [fileListss, setFileListss] = useState("");
  const [image, setImage] = useState("");
  const [userInf, setUserInf] = useState("");
  const [rankUser, setRankUser] = useState("");
  const [branch, setBranch] = useState([]);
  const [form] = Form.useForm();
  const handleChangeImage = (file) => {
    setFileListss(file.fileList);
    uploadImage(fileListss);
  };
  const uploadImage = async (values) => {
    for (var i = 0; i <= values.length - 1; i++) {
      const data = new FormData();
      data.append("file", values[i].originFileObj);
      data.append("upload_preset", "thanhqn");
      setLoading(true);
      const res = await fetch(
        "https://api.cloudinary.com/v1_1/dca35wzo4/image/upload",
        {
          method: "POST",
          body: data,
        }
      );
      const file = await res.json();
      console.log("file", file.secure_url);
      setImage(file.secure_url);
    }
    setLoading(false);
  };
  const onFinish = async (values) => {
    const dataUser = JSON.parse(localStorage.getItem("Authorization1"))?.user;
    const update = {
      fullname: values.username,
      branchId: values.gender,
      image: image,
    };
    console.log("update", update);
    await userApi.updateUserProfile(dataUser.id, update);
    message.success("Cập nhật thông tin thành công");
  };

  useEffect(() => {
    (async () => {
      const dataUser = JSON.parse(localStorage.getItem("Authorization1"))?.user;
      const bra = await branchApi.getBranchAll();
      setBranch(bra);
      const user = await userApi.getUserById(dataUser.id);
      setUserInf(user);
      const rank = getUserRank(Number(user?.point));
      setRankUser(rank);
      form.setFieldsValue({
        username: user.fullname,
        gender: user.branch.name,
      });
    })();
  }, []);
  return (
    <div className="edit-profile">
      <Row className="content">
        <Col span={24} className="inf-user">
          <Row className="user">
            <Col span={8} className="image">
              <div className="avatar-user">
                <img src={userInf?.image || ""} />
              </div>
              <div className="upload">
                <Upload
                  action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                  listType="picture"
                  maxCount={1}
                  onChange={handleChangeImage}
                >
                  <Button icon={<UploadOutlined />}>Upload (Max: 1)</Button>
                </Upload>
              </div>
            </Col>
            <Col span={13} className="general-inf">
              <Col span={24} className="inf1">
                Chào mừng{" "}
                <Popover content={userInf?.fullname || ""}>
                  {userInf?.fullname || ""}
                </Popover>
              </Col>
              <Col span={24} className="inf2">
                Điểm:{" "}
                <Popover content={userInf?.point || ""}>
                  {userInf?.point || ""}
                </Popover>
              </Col>
              <Col span={24} className="inf3">
                Hạng: <span className="green">{rankUser.title}</span>
              </Col>
              <Col span={24} className="inf4">
                Cần thêm 45 điểm lên hạng:
                <div className="aqua rank" onClick={() => rank()}>
                  <i class="bx bx-info-circle"></i>
                  <a>Quan tâm</a>
                </div>
              </Col>
            </Col>
          </Row>
        </Col>
        <Col span={16} className="edit-user">
          <Col className="title">Chỉnh sửa thông tin cá nhân</Col>
          <Col classNam="edit">
            <Form
              form={form}
              name="complex-form"
              onFinish={onFinish}
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
            >
              <Form.Item label="Họ tên">
                <Form.Item
                  name="username"
                  noStyle
                  rules={[{ required: true, message: "Username is required" }]}
                >
                  <Input style={{ width: 250 }} placeholder="Please username" />
                </Form.Item>
              </Form.Item>
              <Form.Item
                name="gender"
                label="Lựa chọn cơ sở"
                rules={[{ required: true, message: "Hãy chọn cơ sở!" }]}
              >
                <Select style={{ width: 250 }} placeholder="Chọn một cơ sở">
                  {branch?.map((data, key) => {
                    return (
                      <Option key={key} value={data.id}>
                        {data.name}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
              <div className="acction-button">
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    Đồng ý
                  </Button>
                </Form.Item>
                <Form.Item>
                  <Button type="primary">Hủy</Button>
                </Form.Item>
              </div>
            </Form>
          </Col>
        </Col>
      </Row>
    </div>
  );
};

export default EditPorfileUser;
