import "./style.scss";
import { useEffect, useState } from "react";
import { Button, Col, Row } from "antd";
import useApi from "../../../../api/user";
import { getUserRank } from "../../../../utils/champion-user";
const UserInfor = () => {
  const [userInfor, setUserInfor] = useState("");
  const [rankUser, setRankUser] = useState("");

  useEffect(() => {
    (async () => {
      const userInf = JSON.parse(localStorage.getItem("Authorization1")).user;
      const user = await useApi.getUserById(userInf.id);
      setUserInfor(user);
      const rank = getUserRank(Number(user?.point));
      setRankUser(rank);
    })();
  }, []);
  return (
    <div className="user-infor">
      <Row className="user-content">
        <Col span={24} className="user-general">
          <div className="user-default">
            <Col span={5} className="avatar">
              <img src={userInfor.image} />
            </Col>
            <Col span={24} className="username">
              <span>{userInfor.fullname}</span>
              <div className="img-rank">
                <img src={rankUser.img} />
                <span>{rankUser.title}</span>
              </div>
            </Col>
          </div>
          <div span={24}>Trả lời hay nhất:</div>
          <div span={24}>
            <span>Điểm: </span>
            <span>{userInfor.point}</span>
          </div>
        </Col>
        <Col span={24} className="user-event">
          <div span={24} className="title-event">
            <span>Sự kiện</span>
          </div>
          <div span={24} className="content-event">
            <div className="event1">
              <i class="bx bxs-flag-alt"></i>
              <span className="red">Đua top nhận quà 12/2021</span>
            </div>
            <div className="event2">
              <i class="bx bxs-flag-alt"></i>
              <span className="red">Quay tiktok - nhận quà chóp</span>
            </div>
          </div>
        </Col>

        <Col span={24} className="start-ask">
          <div className="ask-title">Bạn muốn hỏi điều gì?</div>
          <Button>ĐẶT CÂU HỎI</Button>
        </Col>
        <Col span={24} className="question-new"></Col>
      </Row>
    </div>
  );
};

export default UserInfor;
