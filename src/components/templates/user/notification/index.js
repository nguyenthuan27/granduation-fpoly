import "./style.scss";
import { Col, Row, Spin } from "antd";
import notifiApi from "../../../../api/notifi";
import { useHistory } from "react-router";
import { useEffect, useState } from "react";
import NotiItem from "./notifi";
const Notification = ({ loading, currentNotifi, setIsLoad }) => {
  const history = useHistory();
  const notifiDetail = (id) => {
    if (id == null) return;
    history.push("/post-detail/" + id);
  };
  const reset = (type) => {
    let elements = document.getElementsByClassName("notseen"); // get all elements
    for (let i = 0; i < elements.length; i++) {
      if (type == false) {
        elements[i].style.backgroundColor = "rgba(0,0,0,0.2)";
      } else if (type == true) {
        elements[i].style.backgroundColor = "white";
      }
    }
  };
  return (
    <div className="notification">
      <Row span={24} className="content">
        <Col span={24} className="title">
          Thông báo
        </Col>
        <Col span={24} className="content-notifi">
          {loading ? (
            <Spin size="large"></Spin>
          ) : (
            <>
              {currentNotifi?.map((item) => (
                <div className="next">
                  <NotiItem
                    {...item}
                    key={item.id}
                    comfirm={(type) => reset(type)}
                    setIsLoad={setIsLoad}
                  />
                </div>
              ))}
            </>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default Notification;
