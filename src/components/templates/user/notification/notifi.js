import "./style.scss";
import { Col, Row, Spin } from "antd";
import notifiApi from "../../../../api/notifi";
import { useHistory } from "react-router";
import { useEffect, useState } from "react";
const NotiItem = (props) => {
  const { title, content, idpost, type, comfirm, idnotification, setIsLoad } =
    props;
  const history = useHistory();

  const notifiDetail = async (idpost, idnotification) => {
    if (idpost == null) return;
    const rep = await notifiApi.updateNotiById(idnotification);
    setIsLoad(rep);
    history.push("/post-detail/" + idpost);
  };
  useEffect(() => {
    comfirm(type);
  }, [type]);
  return (
    <>
      <Row className={type == true ? "notidetail" : "notseen"}>
        <Col
          span={24}
          className="conent-notifi"
          onClick={() => notifiDetail(idpost, idnotification)}
        >
          {title}: <a>{content}</a>
        </Col>
        <Col span={24} className="time-noti">
          1 giờ trước
        </Col>
      </Row>
    </>
  );
};

export default NotiItem;
