import { Col, Row } from "antd";
import "./style.scss";
import { useHistory } from "react-router-dom";
import Logout from "../../../../pages/login/Logout";
const UserMenu = () => {
  const history = useHistory();
  const userDetail = (id) => {
    history.push("/user/profile");
  };
  const editUser = () => {
    history.push("/user/edit");
  };
  return (
    <div className="user-menu">
      <Row span={24}>
        <Col span={24} className="menu-item" onClick={() => userDetail()}>
          <i class="bx bxs-right-arrow"></i>
          <span>Trang cá nhân</span>
        </Col>
        {/* <Col span={24} className="menu-item">
          <i class="bx bxs-right-arrow"></i>
          <span>Câu hỏi đã lưu</span>
        </Col> */}
        <Col span={24} className="menu-item" onClick={() => editUser()}>
          <i class="bx bxs-right-arrow"></i>
          <span>Chỉnh sửa thông tin cá nhân</span>
        </Col>
        <Col span={24} className="logout">
          <Logout />
        </Col>
      </Row>
    </div>
  );
};

export default UserMenu;
