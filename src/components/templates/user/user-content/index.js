import "./style.scss";
import { Button, Col, Row, Select } from "antd";
import AskTemplate from "../../../ask";
import reportApi from "../../../../api/report";
import { useEffect, useState } from "react";
import useApi from "../../../../api/user";
import { getUserRank } from "../../../../utils/champion-user";
import apiUser from "../../../../api/user";
const { Option } = Select;
const UserContent = () => {
  const [report, setReport] = useState([]);
  const [isModalAsk, setIsModalAsk] = useState(false);
  const [userInfor, setUserInfor] = useState("");
  const [rankUser, setRankUser] = useState("");
  const [reportId, setReportId] = useState("");

  const showModal = () => {
    setIsModalAsk(true);
  };
  const getReport = async () => {
    let data = await reportApi.reportReply(reportId || 1);
    setReport(data);
  };
  const hanleFilter = (value) => {
    setReportId(value);
  };
  useEffect(() => {
    getReport();
  }, [reportId]);
  useEffect(() => {
    (async () => {
      const userInf = JSON.parse(localStorage.getItem("Authorization1")).user;
      const user = await apiUser.getUserById(userInf.id);
      setUserInfor(user);
      getReport(userInf.id);
      const rank = getUserRank(Number(user?.point));
      setRankUser(rank);
    })();
  }, []);

  return (
    <>
      <Row className="user-content">
        <Col span={21} className="user-general">
          <div className="user-default">
            <Col span={5} className="avatar">
              <img src={userInfor.image} />
            </Col>
            <Col span={24} className="username">
              <span>{userInfor?.fullname || ""}</span>
              <div className="img-rank">
                <img src={rankUser?.img || ""} />
                <span>{rankUser?.title || ""}</span>
              </div>
            </Col>
          </div>
          <div span={24}>Trả lời hay nhất:</div>
          <div span={24}>
            <span>Điểm: </span>
            <span>{userInfor?.point || ""}</span>
          </div>
        </Col>
        <Col span={21} className="start-ask">
          <div className="ask-title">Bạn muốn hỏi điều gì?</div>
          <Button onClick={showModal}>ĐẶT CÂU HỎI</Button>
        </Col>
        <Col span={21} className="user-top">
          <div className="top-title">THÀNH VIÊN HĂNG HÁI NHẤT</div>
          <div className="top-content">
            <div className="top-fillter">
              <Select
                defaultValue="Trong ngày"
                onChange={(value) => hanleFilter(value)}
              >
                <Option value="1">Trong ngày</Option>
                <Option value="2">Trong tuần</Option>
                <Option value="3">Trong tháng</Option>
              </Select>
            </div>
            <div className="ratings-user">
              {report?.map((data) => (
                <Col span={24} className="list-user">
                  <div className="infor-user">
                    <img src={data?.users.image} />
                    <span>{data?.users.fullname}</span>
                  </div>
                  <span className="point">{data?.count} Reply</span>
                </Col>
              ))}
            </div>
          </div>
        </Col>
      </Row>
      <AskTemplate isModalAsk={isModalAsk} setIsModalAsk={setIsModalAsk} />
    </>
  );
};
export default UserContent;
