import { Avatar, Button, Col, Empty, Row, Spin, Tabs, Tooltip } from "antd";
import {
  CalendarOutlined,
  DislikeFilled,
  DislikeOutlined,
  EditOutlined,
  FormOutlined,
  LikeOutlined,
  NotificationOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import "./style.scss";
import * as moment from "moment";
import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { getUserRank } from "../../../../utils/champion-user";
import reportApi from "../../../../api/report";
import PostEdit from "../../post/post-edit";
import apiUser from "../../../../api/user";
const { TabPane } = Tabs;

const UserInforDetail = () => {
  const [rankUser, setRankUser] = useState({});
  const [isShowModal, setIsShowModal] = useState(false);
  const [idpost, setIdpost] = useState("");
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState("");
  const [myReport, setMyReport] = useState("");
  const [myQe, setMyQE] = useState("");
  const history = useHistory();
  const [post, setPost] = useState();
  const editUser = () => {
    history.push("/user/edit");
  };

  const getQuestionDetail = (id) => {
    history.push("/post-detail/" + id);
  };
  const getReport = async (id) => {
    setLoading(true);
    const data = await reportApi.getMyInteractive(id);
    setMyReport(data);
    setMyQE(data?.listrDetailedComment);
    setLoading(false);
  };

  const openEdit = (value) => {
    setIdpost(value);
    setIsShowModal(true);
  };
  useEffect(() => {
    (async () => {
      const userInf = JSON.parse(localStorage.getItem("Authorization1")).user;
      const user = await apiUser.getUserById(userInf.id);
      setUser(user);
      getReport(userInf.id);
      const rank = getUserRank(Number(user?.point));
      setRankUser(rank);
    })();
  }, []);

  useEffect(() => {
    if (myReport != undefined && myReport.listrDetailedComment != undefined) {
      setMyQE((dl) => {
        return dl.map((value, key) => {
          return value?.idposts === post?.idposts ? post : value;
        });
      });
    }
  }, [post]);
  return (
    <div className="container">
      <div className="background"></div>
      <Row className="user-infor-detail">
        <div className="content">
          <div className="avatar">
            <img src={user.image} />
          </div>
          <div className="info">
            <div className="userName">{user.email}</div>
            <div className="name">{user.fullname}</div>
            <div className="date">
              <CalendarOutlined />
              <span> Gia nhập từ: </span>
              {moment(`${user.create_at}`).format("YYYY/MM/DD kk:mm:ss")}
            </div>
            <div className="rank">
              <img src={rankUser.img} />
              <div className="rank-items">
                <span>{rankUser.title}</span> : <a>{user.point} Điểm</a>
              </div>
            </div>
          </div>
          <div>
            <Button
              type="primary"
              icon={<EditOutlined />}
              onClick={() => editUser()}
            >
              Chỉnh sửa
            </Button>
          </div>
        </div>
      </Row>
      <Row className="content-detail">
        <Col span={4} className="statistical">
          <div className="statistical-content">
            <span>
              <QuestionCircleOutlined /> Số câu hỏi đặt ra:
              {myQe?.length || 0}
            </span>
            <br />
            <br />
            <span>
              <FormOutlined /> Số câu trả lời:{" "}
              {myReport?.listrComment?.length || 0}
            </span>
            <br />
            <br />
            <span>
              <NotificationOutlined /> Số lần cảnh cáo: {myQe?.length || 0}
            </span>
          </div>
        </Col>
        <Col span={8} className="thongSo1">
          <Tabs defaultActiveKey="1">
            <TabPane tab="Câu hỏi" key="1" className="itemTab">
              {loading ? (
                <Spin size="large"></Spin>
              ) : (
                <div className="question">
                  {myQe?.length ? (
                    myQe.map((data) => (
                      <div className="your-post">
                        <Row className="post-inf">
                          <Col span={12}>
                            <div className="card">
                              <div className="tag">Môn: Java</div>
                              <div className="create">
                                Thời gian đăng:
                                {moment(data.createAt).format(
                                  "YYYY/MM/DD kk:mm:ss"
                                )}
                              </div>
                            </div>
                          </Col>
                          <Col span={2}>
                            <div className="score">30đ</div>
                          </Col>
                        </Row>
                        <Row className="title-post">
                          <div onClick={() => getQuestionDetail(data?.idposts)}>
                            {data.tilte}
                          </div>
                        </Row>
                        <Row className="content-interact">
                          <Col className="interact">
                            <Tooltip key="comment-basic-like" className="like">
                              <span>
                                <LikeOutlined />
                                <span className="comment-action">
                                  {data?.like}
                                </span>
                              </span>
                            </Tooltip>

                            <Tooltip
                              key="comment-basic-dislike"
                              className="dislike"
                            >
                              <span>
                                <DislikeFilled />
                                <span className="comment-action">
                                  {data?.dislike}
                                </span>
                              </span>
                            </Tooltip>
                          </Col>
                          <Col className="update">
                            <Button
                              type="primary"
                              onClick={() => openEdit(data)}
                            >
                              <i class="bx bx-edit-alt"></i>
                              <span>Sửa bài viết</span>
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    ))
                  ) : (
                    <Empty
                      className="empty"
                      description="No questions answered yet"
                      image={null}
                      style={{
                        margin: "10px 30px",
                        color: "black",
                        fontSize: "40px",
                      }}
                    ></Empty>
                  )}
                </div>
              )}
            </TabPane>
            <TabPane tab="Câu trả lời" key="2" className="itemTab">
              {loading ? (
                <Spin size="large"></Spin>
              ) : (
                <div>
                  {myReport?.listrComment?.length ? (
                    myReport?.listrComment.map((reply) => (
                      <div className="your-cmt">
                        <Row className="content-cmt">
                          <Col span={24} className="post-inf">
                            <span>ID câu hỏi:</span>
                            <a
                              onClick={() => getQuestionDetail(reply?.idposts)}
                            >
                              {reply?.idposts}
                            </a>
                          </Col>
                          <Col span={24} className="date_create">
                            Đã trả lời lúc:
                            <span>
                              {moment(`${reply?.createAt || ""}`).format(
                                "YYYY/MM/DD kk:mm:ss"
                              )}
                            </span>
                          </Col>
                          <Col span={24} className="cmt-content">
                            <b>Câu trả lời:</b>
                            <span>{reply?.content}</span>
                          </Col>
                        </Row>
                      </div>
                    ))
                  ) : (
                    <Empty
                      className="empty"
                      description="No questions answered yet"
                      image={null}
                      style={{
                        margin: "10px 30px",
                        color: "black",
                        fontSize: "40px",
                      }}
                    ></Empty>
                  )}
                </div>
              )}
            </TabPane>
          </Tabs>
        </Col>
      </Row>
      <PostEdit
        idpost={idpost}
        setPost={setPost}
        isModalAsk={isShowModal}
        setIsModalAsk={setIsShowModal}
      />
    </div>
  );
};

export default UserInforDetail;
