import "./style.scss";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import MDEditor from "@uiw/react-md-editor";
import {
  Col,
  Row,
  Select,
  BackTop,
  Input,
  Button,
  Modal,
  Checkbox,
  Upload,
  Switch,
  Form,
  message,
  notification,
} from "antd";
import React, { useEffect, useState } from "react";
import "katex/dist/katex.css";
import moment from "moment";
import { getSubject } from "../../../utils/champion-user";
import { validate } from "@babel/types";
import api from "../../../api/post";
import apiBlog from "../../../api/blog";
const { Option } = Select;

const PostBlog = (props) => {
  const [image, setImage] = useState("");
  const { isModalAsk, setIsModalAsk } = props;
  const [content, setContent] = useState("");
  const [fileList, setFileList] = useState("");
  const [fileListss, setFileListss] = useState("");
  const [previewVisible, setPreviewVisible] = useState(false);
  const [isComfirm, setIsComfirm] = useState(false);
  const [option, setOption] = useState([]);
  const [checked, setChecked] = useState(true);
  const [loading, setLoading] = useState(false);
  const dataSubject = async () => {
    const data = await api.getSubject();
    const listSubject = data?.map((list) => {
      return { value: list.id, label: list.name };
    });
    setOption(listSubject);
  };

  const handleChange = () => {
    setIsModalAsk(false);
  };

  const handlePreview = (file) => {
    setPreviewVisible(true);
    setFileList(file.url || file.thumbUrl);
  };
  const handleChangeImage = (file) => {
    setFileListss(file.fileList);
    uploadImage(fileListss);
  };
  const handleCancelImage = () => {
    setPreviewVisible(false);
  };

  useEffect(() => {
    dataSubject();
  }, []);

  const uploadImage = async (values) => {
    for (var i = 0; i <= values.length - 1; i++) {
      const data = new FormData();
      data.append("file", values[i].originFileObj);
      data.append("upload_preset", "thanhqn");
      setLoading(true);
      const res = await fetch(
        "https://api.cloudinary.com/v1_1/dca35wzo4/image/upload",
        {
          method: "POST",
          body: data,
        }
      );
      const file = await res.json();
      // if (datareturn == "") {
      //   datareturn += file?.secure_url;
      // }
      // else {
      //   datareturn += "," + file?.secure_url;
      // }
      setImage([...image, file?.secure_url]);
      setContent(content + "<br/>![](" + file?.secure_url + ")");
    }
    setLoading(false);
  };
  const onFinish = async (values) => {
    if (fileListss.length > 3) {
      message.info("Số Lượng Ảnh Của 1 câu hỏi phải <=3");
    } else {
      const file = await uploadImage(fileListss);
      const data = {
        id: 0,
        type: false,
        anonymus: false,
        display_status: true,
        content: {
          id: 1,
          content: content,
          img: null,
          title: values.title,
        },
        tag: values.subject.map((data) => {
          return { id: Number(data), name: getSubject(Number(data)) };
        }),
        status: 1,
        userId: JSON.parse(localStorage.getItem("Authorization1")).user.id,
      };
      await apiBlog.createBlog(data);
      setLoading(false);
      setIsModalAsk(false);
      message.info("Bài viết của bạn đã được tải lên thành công!");
    }
  };
  return (
    <>
      <Modal
        centered
        className="details-modal"
        visible={isModalAsk}
        footer={null}
        width={800}
        closable={true}
        onCancel={handleChange}
        title="Hãy viết nhưng thứ mà bạn muốn chia sẻ"
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Row className="content">
            <Col className="selection" span={24}>
              <Form.Item
                name="subject"
                rules={[{ required: true, message: "Subject is required" }]}
              >
                <Select
                  mode="multiple"
                  placeholder="Please select"
                  className="selection-op"
                >
                  {option.map((data, key) => {
                    return (
                      <Option key={key} value={data.value}>
                        {data.label}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
            </Col>
            <Col class="title-content" span={24}>
              <Form.Item
                name="title"
                rules={[
                  { required: true, message: "Title is required" },
                  { type: "string", max: 50 },
                ]}
              >
                <Input placeholder="Nhập tiêu đề của câu hỏi" />
              </Form.Item>
            </Col>
            <Col className="content-post-detail">
              <MDEditor value={content} onChange={setContent} preview="edit" />
              <Col className="content-post-tuto">
                <div className="content-post-tuto-code">
                  <span>```</span>
                  <div className="tuto-code">code</div>
                  <span>```</span>
                </div>
                <div className="content-post-tuto-bold">
                  <span>**</span>
                  <div className="tuto-bold">bold</div>
                  <span>**</span>
                </div>
                <div className="content-post-tuto-italic">
                  <span>*</span>
                  <div className="tuto-italic">italic</div>
                  <span>*</span>
                </div>
                <div className="content-post-tuto-quote">
                  <span>></span>
                  <div className="tuto-quote">quote</div>
                </div>
                <div className="content-post-tuto-down-line">
                  <span>\</span>
                  <div className="tuto-down-line">
                    enter key to down the line
                  </div>
                </div>
                <div className="content-post-tuto-syntax">
                  <a
                    target="_blank"
                    href="https://www.markdownguide.org/basic-syntax/"
                  >
                    markdown syntax
                  </a>
                </div>
              </Col>
              {content == null || content == "" ? null : (
                <Col className="content-post-preview">
                  <MDEditor.Markdown source={content} />
                </Col>
              )}
            </Col>
            <Col className="upload-file" span={24}>
              <div>
                <Upload
                  // action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                  listType="picture-card"
                  onPreview={handlePreview}
                  onChange={handleChangeImage}
                >
                  <div>
                    <i class="bx bx-upload"></i>
                  </div>
                </Upload>
                <Modal
                  visible={previewVisible}
                  footer={null}
                  onCancel={handleCancelImage}
                >
                  <img alt="example" style={{ width: "100%" }} src={fileList} />
                </Modal>
              </div>
            </Col>
            <Col className="submit-post" span={24}>
              <div>
                <Button type="primary" htmlType="submit" loading={loading}>
                  {loading ? "Loading..." : "Gửi câu hỏi"}
                </Button>
              </div>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default PostBlog;
