import "./style.scss";
import { Button, Col, Comment, Tooltip, Avatar, Form, Input, List, Upload } from "antd";
import ReactMarkdown from "react-markdown";
import MDEditor from "@uiw/react-md-editor";
import { useState } from "react";
import { UploadOutlined } from "@ant-design/icons";

const { TextArea } = Input;

const FormCommnent = (props) => {
  const { handleChangeImage, closePreview, openPreview, setIsPreview, isPreview, onChange, onSubmit, value, setValue, isLoading } = props;

  return (
    <div className="form-comment">
      {
        isPreview == true ?
          <div className="preview-comment">
            <MDEditor.Markdown source={value} />
          </div>
          :
          <div>
            <TextArea
              rows={4}
              name="content"
              className="textarea"
              autoFocus
              value={value}
              onChange={onChange}
            />
            <div>
              <Upload
                listType="picture"
                maxCount={1}
                onChange={handleChangeImage}
              >
                <Button className="button-upload" icon={<UploadOutlined />}>Đăng ảnh (Max: 1)</Button>
              </Upload>
            </div>
          </div>
      }
      <Button htmlType="submit" onClick={onSubmit} loading={isLoading}>
        {isLoading ? "Đang gửi..." : "Gửi câu trả lời"}
      </Button>
      {
        isPreview == true ?
          <Button style={{ marginLeft: 15 }} onClick={closePreview}>
            Tiếp tục viết
          </Button>
          :
          <Button style={{ marginLeft: 15 }} onClick={openPreview}>
            Xem trước
          </Button>
      }
    </div>
  );
};

export default FormCommnent;
