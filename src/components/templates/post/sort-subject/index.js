import "./style.scss";
import { Col, Row, Menu, Select, Form, Checkbox } from "antd";

const SortSubject = (props) => {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    props.onChangeSubjectSortData(values);
  };
  const onChangeFilter = () => {
    form.validateFields()
      .then((values) => {
        props.onChangeSubjectSortData(values);
      })
      .catch((errorInfo) => {});
  };

  return (
    <div className="menu-sort-subject">
      <Form onFinish={onFinish} form={form}>
        <Form.Item
          name="classes"
          className="filter-group"
          onChange={onChangeFilter}
        >
          <Checkbox.Group>
            <Menu mode="inline">
              <Menu.Item key="1">
                <Checkbox value="3">
                  <i class="bx bx-grid-small"></i>C++
                </Checkbox>
              </Menu.Item>
              <Menu.Item key="2">
                <Checkbox value="1">
                  <i class="bx bxl-spring-boot"></i>Java
                </Checkbox>
              </Menu.Item>
              <Menu.Item key="3">
                <Checkbox value="5">
                  <i class="bx bxs-data"></i>Database
                </Checkbox>
              </Menu.Item>
              <Menu.Item key="4">
                <Checkbox value="4">
                  <i class="bx bxl-react"></i>JavaScript
                </Checkbox>
              </Menu.Item>
              <Menu.Item key="5">
                <Checkbox value="2">
                  <i class="bx bxl-php"></i>PHP
                </Checkbox>
              </Menu.Item>
            </Menu>
          </Checkbox.Group>
        </Form.Item>
      </Form>
    </div>
  );
};
export default SortSubject;
