import "./style.scss";
import {
  Button,
  Col,
  Input,
  message,
  Modal,
  Radio,
  Row,
  Space,
  Form,
} from "antd";
import { useState } from "react";
import { initReasonList } from "../../../../utils/champion-user";
import reportApi from "../../../../api/report";
import Notifi from "../../../comfirm/notifi";
const FormItem = Form.Item;
const ReportPost = (props) => {
  const { isModalAsk, setIsModalAsk, questionId } = props;
  const [isCheck, setIsCheck] = useState(false);
  const [reason, setReason] = useState("");
  const onChange = (e) => {
    setReason({
      value: e.target.value,
    });
    console.log("reason", e.target.value);
  };
  const handleCancel = () => {
    setIsModalAsk(false);
  };
  const onFinish = (values) => {
    setIsCheck(true);
  };
  return (
    <>
      <Modal
        //centered
        className="report-post-modal"
        visible={isModalAsk}
        closable={true}
        onCancel={handleCancel}
        footer
      >
        <Form
          name="basic"
          onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Row className="report-content">
            <Col span={23} className="title">
              <div>Lý do báo cáo vi phạm?</div>
            </Col>
            <Row>
              <Form.Item
                name="radio-button"
                rules={[
                  { required: true, message: "Bạn chưa chọn lý do vi phạm!" },
                ]}
              >
                <Radio.Group onChange={onChange}>
                  <Space direction="vertical">
                    {initReasonList.map((value) => {
                      return (
                        <Radio key={value.id} value={value.content}>
                          {value.content}
                        </Radio>
                      );
                    })}
                  </Space>
                </Radio.Group>
              </Form.Item>
            </Row>
            <Col span={24} className="button">
              <Form.Item>
                <Button
                  htmlType="submit"
                  key="submit"
                  type="primary"
                  style={{ borderRadius: 5, width: 100 }}
                >
                  Gửi đi
                </Button>

                <Button
                  type="primary"
                  style={{ borderRadius: 5, width: 100 }}
                  onClick={() => handleCancel()}
                >
                  Hủy
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
      <Notifi
        isCheck={isCheck}
        setIsCheck={setIsCheck}
        content={reason?.value}
        questionId={questionId}
        setIsModalAsk={setIsModalAsk}
      />
    </>
  );
};

export default ReportPost;
