import "./style.scss";
import { Button, Col, Comment, Tooltip, Avatar, Form, Input, List } from "antd";
import React, { useEffect, useState } from "react";
import {
  DislikeOutlined,
  LikeOutlined,
  DislikeFilled,
  LikeFilled,
  UserOutlined,
} from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import * as moment from "moment";

const PostContent = (props) => {
  const {
    question,
    tag,
    updateDay,
    usersPost,
    like,
    disLike,
    comment,
    anonymus,
    id,
  } = props;
  const history = useHistory();
  const [likes, setLikes] = useState(1);
  const [dislikes, setDislikes] = useState(0);
  const [action, setAction] = useState("");

  const liked = () => {
    setLikes(2);
    setDislikes(0);
    setAction("liked");
  };
  const dislike = () => {
    setLikes(1);
    setDislikes(1);
    setAction("disliked");
  };

  const onPostDetail = () => {
    history.push("/post-detail/" + id);
  };

  return (
    <>
      <div className="item-content">
        <Col span={24} className="content-info" onClick={() => onPostDetail()}>
          <div className="infor-image">
            {anonymus == 1 ? (
              <Avatar src={usersPost.image} />
            ) : (
              <Avatar size="large" icon={<UserOutlined />} />
            )}
          </div>
          <div className="infor-details">
            <Col span={24} className="details">
              <span>
                {anonymus == 1 ? (
                  <b>{usersPost.fullname}</b>
                ) : (
                  <b className="anonymus">
                    <i class="bx bx-question-mark bx-tada bx-rotate-90"></i>
                  </b>
                )}
              </span>
              <span>
                Môn:
                {tag?.map((data) => (
                  <span>{data.name}</span>
                ))}
              </span>
            </Col>
            <Col span={24} className="details-time">
              <span>
                {/* {moment(question.updateDay).format("YYYY/MM/DD kk:mm:ss")} */}
                {question.updateDay}
              </span>
            </Col>
          </div>
          <div className="infor-point">{question.point}</div>
        </Col>
        <Col span={24} className="content-title">
          <div></div>
          {question.title}
        </Col>
        <Col className="content-interact">
          <div className="interact">
            <Tooltip key="comment-basic-like" className="like">
              <span onClick={() => liked()}>
                <LikeOutlined />
                <span className="comment-action">{disLike}</span>
              </span>
            </Tooltip>

            <Tooltip key="comment-basic-dislike" className="dislike">
              <span onClick={() => dislike()}>
                <DislikeOutlined />
                <span className="comment-action">{like}</span>
              </span>
            </Tooltip>
          </div>
          <div className="reply">
            <Button
              htmlType="submit"
              type="primary"
              // onClick={() => onPostDetail()}
            >
              {comment} Reply
            </Button>
          </div>
        </Col>
      </div>
    </>
  );
};

export default PostContent;
