import "./style.scss";
import { Col, Row } from "antd";
import PostDetail from "../detail-post";
import UserInfor from "../../user/user-infor";
import { useEffect, useState } from "react";
import postApi from "../../../../api/post";

const PostUser = () => {
  const [questionId,setQuestionId] = useState('');
  const [questionIds,setQuestionIds] = useState('');
  const getUrl = window.location.href.split("/");
  const postId = getUrl[4];
  
  const getPost = async () => {
    const dataPost = await postApi.getPostById(postId);
    setQuestionId(dataPost?.question.id);
    setQuestionIds(dataPost?.id)
    setQuestionId(dataPost?.question.id);
  }
  
  useEffect(() => {
    getPost();
  },[postId])
  return (
    <div className="post">
      <Row className="post-detail">
        <Col span={16} className="post-conent">
         <PostDetail  Post={questionIds} questionId={questionId}  />
        </Col>
        <Col span={8} className="trending-content">
         <UserInfor />
        </Col>
      </Row>
    </div>
  )
}
export default PostUser;