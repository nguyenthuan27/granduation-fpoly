import "./style.scss";
import {
  Col,
  Comment,
  Tooltip,
  Avatar,
  Row,
  message,
  notification,
} from "antd";
import React, { useState, useEffect } from "react";
import axios from "axios";
import MDEditor from "@uiw/react-md-editor";
import {
  DislikeOutlined,
  LikeOutlined,
  DislikeFilled,
  LikeFilled,
  UserOutlined,
} from "@ant-design/icons";
import * as moment from "moment";
import questionApi from "../../../../api/question";
import apiUser from "../../../../api/user";
import FormCommnent from "../form-comment";
import CommentList from "../list-comment";
import socketIOClient from "socket.io-client";
import ReportPost from "../report-post";
import { useHistory } from "react-router-dom";
const socket = socketIOClient(process.env.REACT_APP_API_SOCKET_IO);
const PostDetail = (props) => {
  const { Post, questionId } = props;
  console.log("post", Post);
  const getRoom = window.location.href.split("/");
  const room = getRoom[getRoom.length - 1];
  socket.emit("joinroom", room);
  const listComment = {
    content: "",
  };
  const [content, setContent] = useState("");
  const [fileList, setFileList] = useState("");
  const [fileListss, setFileListss] = useState("");
  const [comments, setComments] = useState([]);
  const [showReply, setShowReply] = useState(false);
  const [getmsg, setGetmsg] = useState(null);
  const [dataQuestion, setDataQuestion] = useState([]);
  const [dataComment, setDataComment] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [action, setAction] = useState("");
  const userInf = JSON.parse(localStorage.getItem("Authorization1")).user;
  const [isPreview, setIsPreview] = useState(false);

  const openPreview = () => {
    setIsPreview(true);
  };
  const closePreview = () => {
    setIsPreview(false);
  };
  const like = async () => {
    const param = {
      like: true,
      postId: Post,
      userId: userInf.id,
      dislike: false,
    };
    await questionApi.feelQuestion(param);
    setAction("like");
    message.success("Like success!");
  };
  const dislike = async () => {
    const param = {
      like: false,
      postId: Post,
      userId: userInf.id,
      dislike: true,
    };
    await questionApi.feelQuestion(param);
    setAction("dislike");
    message.success("Dislike success!");
  };
  const handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    setContent(value);
  };
  const handleChangeImage = (file) => {
    setFileListss(file.fileList);
    uploadImage(fileListss);
  };
  const uploadImage = async (values) => {
    for (var i = 0; i <= values.length - 1; i++) {
      const data = new FormData();
      data.append("file", values[i].originFileObj);
      data.append("upload_preset", "thanhqn");
      const res = await fetch(
        "https://api.cloudinary.com/v1_1/dca35wzo4/image/upload",
        {
          method: "POST",
          body: data,
        }
      );
      const file = await res.json();
      // setImage([...image, file?.secure_url])
      setContent(content + "<br/>![](" + file?.secure_url + ")");
    }
  };

  useEffect(() => {
    socket.on("msgComment", (data) => {
      console.log(data);
      setGetmsg(data);
      notification.open({
        message: "Thông báo",
        description: "Câu trả lời của bạn đã được gửi thành công!",
      });
    });
    socket.on("msgErrorComment", (data) => {
      console.log(data);
    });
    return () => {
      socket.destroy();
    };
  }, [socket]);
  const handleSubmit = async () => {
    const id = JSON.parse(localStorage.getItem("Authorization1")).user;
    const user = await apiUser.getUserById(id.id);
    if (!content) {
      message.info("Bạn chưa nhập nội dung");
    } else {
      const usersPost = {
        id: user.id,
        username: user.fullname,
        fullname: user.fullname,
        email: user.email,
        point: user.point,
        role: user.role,
        create_at: user.fullname.createAt,
        image: user.image,
      };
      setIsLoading(true);
      const dataReturn = {
        question_id: room,
        user: usersPost,
        content: content,
        type: 1,
        anonymus: 1,
        status: 1,
        img: "anh",
      };
      socket.emit("Comment", dataReturn);
      setIsLoading(false);
    }
  };
  useEffect(() => {
    if (getmsg != null) {
      if (dataComment == null) {
        setDataComment([getmsg]);
        setContent("");
      } else {
        setDataComment([...dataComment, getmsg]);
        setContent("");
      }
    }
  }, [getmsg]);
  const [isModalAsk, setIsModalAsk] = useState(false);
  const showModal = () => {
    setIsModalAsk(true);
  };

  const getData = async () => {
    const params = { id: Post, user: userInf.id };
    const question = await questionApi.getQuestionById(params);
    setDataQuestion(question);
    setDataComment(question.listrComment);
  };

  useEffect(() => {
    getData();
  }, [Post]);

  useEffect(() => {
    getData();
  }, [action]);
  return (
    <>
      <div className="item-detail">
        <Col span={24} className="content-info">
          <div className="infor-image">
            {dataQuestion?.post?.anonymus == 1 ? (
              <Avatar src={dataQuestion?.usersPost?.image} />
            ) : (
              <Avatar size="large" icon={<UserOutlined />} />
            )}
          </div>
          <div className="infor-details">
            <Col span={24} className="details">
              <span>
                {dataQuestion?.post?.anonymus == 1 ? (
                  <b>{dataQuestion?.usersPost?.fullname}</b>
                ) : (
                  <b className="anonymus">
                    <i class="bx bx-question-mark bx-tada bx-rotate-90"></i>
                  </b>
                )}
              </span>
              <span>
                Môn:{" "}
                {dataQuestion?.tag?.map((data) => (
                  <span>{data.name}</span>
                ))}
              </span>
            </Col>
            <Col span={24} className="details-time">
              Đã hỏi lúc:
              {/* {moment(`${dataQuestion?.updateDay || ""}`).format(
                "YYYY/MM/DD kk:mm:ss"
              )} */}
              {dataQuestion?.updateDay}
            </Col>
          </div>
          <div className="infor-point">{dataQuestion.point}</div>
        </Col>
        <Col span={24} className="content-detail">
          <div className="title-content">{dataQuestion?.title || ""}</div>
          <div className="post-content">
            {/* {dataQuestion?.content || ""} */}
            <MDEditor.Markdown source={dataQuestion?.content || ""} />
          </div>
        </Col>
        <Row className="content-interact">
          <Col span={8} className="interact">
            <Tooltip key="comment-basic-like" className="like">
              <span onClick={() => like()}>
                {dataQuestion?.post?.stauts === 1 ? (
                  <LikeFilled />
                ) : (
                  <LikeOutlined />
                )}
                <span className="comment-action">
                  {dataQuestion?.post?.countLike}
                </span>
              </span>
            </Tooltip>

            <Tooltip key="comment-basic-dislike" className="dislike">
              <span onClick={() => dislike()}>
                {dataQuestion?.post?.stauts === 2 ? (
                  <DislikeFilled />
                ) : (
                  <DislikeOutlined />
                )}
                <span className="comment-action">
                  {dataQuestion?.post?.countDis}
                </span>
              </span>
            </Tooltip>
          </Col>
          <Col span={13} className="reply">
            <div className="detail" onClick={() => setShowReply(true)}>
              <i class="bx bxs-comment-detail"></i>
              <span>Trả lời ngay</span>
            </div>
            <div className="follow-content">
              <i class="bx bxs-user-check"> </i>
              Theo dõi
            </div>
            <div className="report-content" onClick={showModal}>
              <i class="bx bxs-flag-alt"></i>
              Báo cáo vi phạm
            </div>
          </Col>
        </Row>
      </div>
      <Col span={24} className="comment">
        {showReply === true && (
          <>
            <Comment
              avatar={
                <Avatar
                  src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                  alt="Han Solo"
                />
              }
              content={
                <FormCommnent
                  isLoading={isLoading}
                  onChange={handleChange}
                  onSubmit={handleSubmit}
                  value={content}
                  setValue={setContent}
                  isPreview={isPreview}
                  setIsPreview={setIsPreview}
                  openPreview={openPreview}
                  closePreview={closePreview}
                  handleChangeImage={handleChangeImage}
                />
              }
            />
          </>
        )}
      </Col>
      <Col span={24} className="list-cmt">
        <CommentList comments={comments} dataComment={dataComment} />
      </Col>
      <ReportPost
        isModalAsk={isModalAsk}
        setIsModalAsk={setIsModalAsk}
        questionId={questionId}
      />
    </>
  );
};
export default PostDetail;
