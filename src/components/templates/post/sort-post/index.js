import { Col, Row, Select } from "antd";
import "./style.scss";
const { Option } = Select;

const SortPost = (props) => {
  const { dataSort, setDataSort } = props;
  const onChangeSort = (sortType) => {
    setDataSort(sortType);
  };
  return (
    <Row className="post-select">
      <div className="select">
        <Select defaultValue="all" onChange={onChangeSort}>
          <Option value="all">Tất cả</Option>
          <Option value="answe">Đã trả lời</Option>
          <Option value="notanswered">Chưa trả lời</Option>
        </Select>
      </div>
      <div className="select">
        <Select defaultValue="all" onChange={onChangeSort}>
          <Option value="all">Tất cả</Option>
          <Option value="post-asc">Thời gian giảm dần</Option>
          <Option value="post-dsc">Thời gian tăng dần</Option>
        </Select>
      </div>
    </Row>
  );
};

export default SortPost;
