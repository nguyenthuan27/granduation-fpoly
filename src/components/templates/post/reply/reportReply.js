import "./style.scss";
import { Button, Col, Form, Modal, Radio, Row, Space } from "antd";
import { useState } from "react";
import { initReasonList } from "../../../../utils/champion-user";
import ComfirmReportReply from "../../../comfirm/comfirmReply";
const ReportReply = (props) => {
  const { isReportReply, setIsReportReply, idReply } = props;
  const [reason, setReason] = useState("");
  const [isChecked, setIsChecked] = useState(false);
  const onChange = (e) => {
    setReason({
      value: e.target.value,
    });
  };
  const handleCancel = () => {
    setIsReportReply(false);
  };
  const onFinish = (values) => {
      setIsChecked(true);
  };
  return (
    <>
      <Modal
        //centered
        className="report-post-modal"
        visible={isReportReply}
        closable={true}
        onCancel={handleCancel}
        footer
      >
        <Form
          name="basic"
          onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Row className="report-content">
            <Col span={23} className="title">
              <div>Lý do báo cáo vi phạm?</div>
            </Col>
            <Row>
              <Form.Item
                name="radio-button"
                rules={[
                  { required: true, message: "Bạn chưa chọn lý do vi phạm!" },
                ]}
              >
                <Radio.Group onChange={onChange}>
                  <Space direction="vertical">
                    {initReasonList.map((value) => {
                      return (
                        <Radio key={value.id} value={value.content}>
                          {value.content}
                        </Radio>
                      );
                    })}
                  </Space>
                </Radio.Group>
              </Form.Item>
            </Row>
            <Col span={24} className="button">
              <Form.Item>
                <Button
                  htmlType="submit"
                  key="submit"
                  type="primary"
                  style={{ borderRadius: 5, width: 100 }}
                >
                  Gửi đi
                </Button>

                <Button
                  type="primary"
                  style={{ borderRadius: 5, width: 100 }}
                  onClick={() => handleCancel()}
                >
                  Hủy
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
      <ComfirmReportReply
        isCheck={isChecked}
        setIsCheck={setIsChecked}
        content={reason?.value}
        setIsReportReply={setIsReportReply}
        idReply={idReply}
      />
    </>
  );
};

export default ReportReply;
