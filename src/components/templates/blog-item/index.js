import "./style.scss";
import { Col, Row, Tooltip } from "antd";
import React from "react";
import { DislikeOutlined, LikeOutlined } from "@ant-design/icons";
import { useHistory } from "react-router";

const BlogItem = (props) => {
  const { disLike, like, tag, usersPost, question, id } = props;
  const history = useHistory();

  const onBlogDetail = (id) => {
    console.log("id", id);
    history.push("/blog-detail/" + id);
  };
  return (
    <>
      <div className="blog-item">
        <Row span={24} className="item-title">
          <Col span={2} className="item-avatar">
            <div className="avatar">
              <img src="./images/avatar.jpg" />
            </div>
          </Col>
          <Col span={22} className="content">
            <Col span={24} className="details">
              <a>{usersPost.fullname}</a>
              <span>
                {tag?.map((data) => (
                  <span>{data.name}</span>
                ))}
              </span>
              <div>{question.createDay}</div>
            </Col>
            <Col span={24} className="details-title">
              <span onClick={() => onBlogDetail(props.id)}>
                {question.title}
              </span>
            </Col>
          </Col>
        </Row>
        <Row span={24} className="interactive">
          <div className="interact">
            <Tooltip key="comment-basic-like" className="like">
              <span>
                <LikeOutlined />
                <span className="comment-action">{like}</span>
              </span>
            </Tooltip>

            <Tooltip key="comment-basic-dislike" className="dislike">
              <span>
                <DislikeOutlined />
                <span className="comment-action">{disLike}</span>
              </span>
            </Tooltip>
          </div>
        </Row>
      </div>
    </>
  );
};

export default BlogItem;
