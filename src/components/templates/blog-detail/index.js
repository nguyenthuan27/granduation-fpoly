import "./style.scss";
import {
  Col,
  Comment,
  Tooltip,
  Avatar,
  Row,
  message,
  notification,
} from "antd";
import React, { useState, useEffect } from "react";
import axios from "axios";
import MDEditor from "@uiw/react-md-editor";
import {
  DislikeOutlined,
  LikeOutlined,
  DislikeFilled,
  LikeFilled,
  UserOutlined,
} from "@ant-design/icons";
import * as moment from "moment";
import blogAapi from "../../../api/blog";
import questionApi from "../../../api/question";

const BlogDetails = () => {
  const getRoom = window.location.href.split("/");
  const room = getRoom[getRoom.length - 1];

  const listComment = {
    content: "",
  };
  const [content, setContent] = useState("");
  const [fileList, setFileList] = useState("");
  const [fileListss, setFileListss] = useState("");
  const [comments, setComments] = useState([]);
  const [showReply, setShowReply] = useState(true);
  const [getmsg, setGetmsg] = useState(null);
  const [dataQuestion, setDataQuestion] = useState([]);
  const [dataComment, setDataComment] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [action, setAction] = useState("");
  const userInf = JSON.parse(localStorage.getItem("Authorization1")).user;
  const [isPreview, setIsPreview] = useState(false);

  const openPreview = () => {
    setIsPreview(true);
  };
  const closePreview = () => {
    setIsPreview(false);
  };
  const like = async () => {
    const param = {
      like: true,
      postId: room,
      userId: userInf.id,
      dislike: false,
    };
    await questionApi.feelQuestion(param);
    setAction("like");
    message.success("Dislike success!");
  };
  const dislike = async () => {
    const param = {
      like: false,
      postId: room,
      userId: userInf.id,
      dislike: true,
    };
    await questionApi.feelQuestion(param);
    setAction("dislike");
    message.success("Like success!");
  };
  const handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    setContent(value);
  };
  const handleChangeImage = (file) => {
    setFileListss(file.fileList);
    uploadImage(fileListss);
  };
  const uploadImage = async (values) => {
    for (var i = 0; i <= values.length - 1; i++) {
      const data = new FormData();
      data.append("file", values[i].originFileObj);
      data.append("upload_preset", "thanhqn");
      const res = await fetch(
        "https://api.cloudinary.com/v1_1/dca35wzo4/image/upload",
        {
          method: "POST",
          body: data,
        }
      );
      const file = await res.json();
      // setImage([...image, file?.secure_url])
      setContent(content + "<br/>![](" + file?.secure_url + ")");
    }
  };

  const getData = async () => {
    const question = await blogAapi.getBlogById(room);
    setDataQuestion(question);
    setDataComment(question.listrComment);
  };

  useEffect(() => {
    getData();
  }, [action]);
  return (
    <>
      <div className="blog-detail">
        <Row className="detail-content">
          <Col span={1} className="action-user">
            <div className="interact">
              <Tooltip key="comment-basic-like" className="like">
                <span onClick={() => dislike()}>
                  <LikeOutlined />
                  <span className="comment-action">{dataQuestion.like}</span>
                </span>
              </Tooltip>

              <Tooltip key="comment-basic-dislike" className="dislike">
                <span onClick={() => like()}>
                  <DislikeOutlined />
                  <span className="comment-action">{dataQuestion.disLike}</span>
                </span>
              </Tooltip>
            </div>
          </Col>
          <Col span={16} className="blog">
            <Row style={{ width: "100%" }}>
              <Col span={24} className="infor-user">
                <Row className="item-title">
                  <Col span={3} className="item-avatar">
                    <div className="avatar">
                      <img src="../images/avatar.jpg" />
                    </div>
                  </Col>
                  <Col span={20} className="content">
                    <Col span={24} className="details">
                      <span>{dataQuestion?.usersPost?.fullname}</span>
                    </Col>
                    <Col span={24} className="user">
                      <div style={{ marginRight: 10 }}>
                        <i class="bx bxs-star"></i>
                        <span style={{ paddingLeft: 5 }}>
                          {dataQuestion?.usersPost?.point}
                        </span>
                      </div>
                      <div>
                        <i class="bx bxs-edit-alt"></i>
                        <span style={{ paddingLeft: 5 }}>10</span>
                      </div>
                    </Col>
                  </Col>
                </Row>
                <Row className="time-current">
                  <Col span={24}>
                    <span style={{ paddingRight: 5 }}>Đã đăng lúc: </span>
                    <span>{dataQuestion?.question?.createDay || ""}</span>
                  </Col>
                  <Col
                    span={24}
                    style={{
                      marginBottom: 30,
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <i class="bx bx-chat"></i>
                    <span style={{ paddingLeft: 5 }}>
                      {dataQuestion?.comment}
                    </span>
                  </Col>
                </Row>
              </Col>
              <Col span={24} className="blog-titles">
                {dataQuestion?.question?.title || ""}
              </Col>
              <Col span={24} className="content-blog">
                <MDEditor.Markdown
                  source={dataQuestion?.question?.content || ""}
                />
              </Col>
            </Row>
          </Col>
          <Col span={7} className=""></Col>
        </Row>
      </div>
    </>
  );
};

export default BlogDetails;
