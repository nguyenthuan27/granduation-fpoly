import "./style.scss";
import { Avatar, Col, Comment, Empty, List, Row } from "antd";
import React, { useEffect, useState } from "react";
import * as moment from "moment";
import ComentBlog from "./comentBlog";


const CommentListBlog = (props) => {
  const { dataComment } = props;
  const [currentReply, setCurrentReply] = useState(5);

  const loadingReply = () => {
    setCurrentReply(currentReply + 5);
  };

  return (
    <div className="list-comment">
      <Row className="comment">
        <Col span={24} className="total-reply">
          <span>{dataComment?.length || ""} CÂU TRẢ LỜI</span>
          <div className="hr"></div>
        </Col>
        <Col className="list-reply" span={24}>
          {
            dataComment?.length
              ? dataComment
                  ?.slice(0, currentReply)
                  .map((reply) => (
                    <ComentBlog
                      {...reply}
                      key={reply.id}
                      size="small"
                    ></ComentBlog>
                  ))
              : null
            // (
            //   <Empty
            //     className="empty"
            //     description="This post doesn't has any reply"
            //     image={null}
            //     style={{ margin: "0px auto", color: "black", fontSize: "30px" }}
            //   ></Empty>
            // )
          }
          {dataComment?.length && currentReply < dataComment?.length ? (
            <div className="load-more" onClick={loadingReply}>
              Load More .........
            </div>
          ) : (
            <></>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default CommentListBlog;
