import { Col, Row, Tooltip } from "antd";
import "./style.scss";
import * as moment from "moment";
import { useEffect, useState } from "react";
import MDEditor from "@uiw/react-md-editor";

const ComentBlog = (props) => {
  const { user, content, createAt, id } = props;
  const [isReportReply, setIsReportReply] = useState(false);
  const showReport = () => {
    setIsReportReply(true);
  };
  return (
    <>
      <Row className="reply-item">
        <Col span={24} className="user-inf">
          <div className="avatar">
            <img src="/images/avatar.jpg" />
          </div>
          <div className="reply-inf">
            <div className="username">{user?.fullname}</div>
            <div className="create-at">
              {moment(`${createAt || ""}`).format("YYYY/MM/DD kk:mm:ss")}
            </div>
          </div>
          <Tooltip placement="topRight" title="Báo cáo">
            <div className="report" onClick={() => showReport()}>
              <i class="bx bxs-flag-alt"></i>
            </div>
          </Tooltip>
        </Col>
        <Col span={24} className="reply-content">
          <MDEditor.Markdown source={content} />
          {/* <div className="content">{content}</div> */}
        </Col>
      </Row>
    </>
  );
};
export default ComentBlog;
