import "./style.scss";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import MDEditor from "@uiw/react-md-editor";
import {
  Col,
  Row,
  Select,
  BackTop,
  Input,
  Button,
  Modal,
  Checkbox,
  Upload,
  Switch,
  Form,
  message,
  notification,
} from "antd";
import React, { useEffect, useState } from "react";
import "katex/dist/katex.css";
import socketIOClient from "socket.io-client";
import moment from "moment";
import api from "../../api/post";
import Comfirm from "../comfirm";
import apiUser from "../../api/user";
import { getSubject } from "../../utils/champion-user";
import { validate } from "@babel/types";
const socket = socketIOClient(process.env.REACT_APP_API_SOCKET_IO);
const { Option } = Select;

const AskTemplate = (props) => {
  const [image, setImage] = useState("");
  const { isModalAsk, setIsModalAsk } = props;
  const [content, setContent] = useState("");
  const [fileList, setFileList] = useState("");
  const [userPost, setUserPost] = useState("");
  const [fileListss, setFileListss] = useState("");
  const [previewVisible, setPreviewVisible] = useState(false);
  const [isComfirm, setIsComfirm] = useState(false);
  const [option, setOption] = useState([]);
  const [checked, setChecked] = useState(true);
  const [loading, setLoading] = useState(false);
  const dataSubject = async () => {
    const idUser = JSON.parse(localStorage.getItem("Authorization1"))?.user;
    const userData = await apiUser.getUserById(idUser?.id || 158);
    setUserPost(userData);
    const data = await api.getSubject();
    const listSubject = data?.map((list) => {
      return { value: list.id, label: list.name };
    });
    setOption(listSubject);
  };

  const handleChange = () => {
    setIsModalAsk(false);
  };

  const handlePreview = (file) => {
    setPreviewVisible(true);
    setFileList(file.url || file.thumbUrl);
  };
  const handleChangeImage = (file) => {
    setFileListss(file.fileList);
    uploadImage(fileListss);
  };
  const handleCancelImage = () => {
    setPreviewVisible(false);
  };

  useEffect(() => {
    dataSubject();
  }, []);

  const contentPost = (content, delta, source, editor) => {
    setContent(editor.getText());
  };

  const postAnonymous = (event) => {
    if (event == false) {
      setIsComfirm(true);
    } else {
      setChecked(true);
    }
  };
  const uploadImage = async (values) => {
    for (var i = 0; i <= values.length - 1; i++) {
      const data = new FormData();
      data.append("file", values[i].originFileObj);
      data.append("upload_preset", "thanhqn");
      setLoading(true);
      const res = await fetch(
        "https://api.cloudinary.com/v1_1/dca35wzo4/image/upload",
        {
          method: "POST",
          body: data,
        }
      );
      const file = await res.json();
      // if (datareturn == "") {
      //   datareturn += file?.secure_url;
      // }
      // else {
      //   datareturn += "," + file?.secure_url;
      // }
      setImage([...image, file?.secure_url]);
      setContent(content + "<br/>![](" + file?.secure_url + ")");
    }
    setLoading(false);
  };
  const onFinish = async (values) => {
    const id = JSON.parse(localStorage.getItem("Authorization1")).user;
    const user = await apiUser.getUserById(id.id);
    const usersPost = {
      id: user.id,
      username: user.fullname,
      fullname: user.fullname,
      email: user.email,
      point: user.point,
      role: user.role,
      create_at: user.fullname.createAt,
      image: user.image,
    };
    if (fileListss.length > 3) {
      message.info("Số Lượng Ảnh Của 1 câu hỏi phải <=3");
    } else {
      if (!content) {
        message.info("Bạn chưa nhập nội dung");
      } else {
        const file = await uploadImage(fileListss);
        const datapost = {
          room: "home",
          id: 0,
          type: 1,
          anonymus: checked ? 1 : 0,
          display_status: checked ? 1 : 0,
          question: {
            id: 1,
            content: content,
            point: Number(values.point),
            img: null,
            title: values.title,
            updateDay: moment().format("DD-MM-YYYY hh:mm:ss"),
            createDay: moment().format("DD-MM-YYYY hh:mm:ss"),
          },
          tag: values.subject.map((data) => {
            return { id: Number(data), name: getSubject(Number(data)) };
          }),
          comment: 0,
          like: 0,
          usersPost: usersPost,
          disLike: 0,
          detailedComment: 0,
        };
        setLoading(true);
        socket.emit("NewBlog", datapost);
        setLoading(false);
        setIsModalAsk(false);
        message.info("Câu hỏi của bạn đã được tải lên thành công!");
      }
    }
  };
  return (
    <>
      <Modal
        centered
        className="details-modal"
        visible={isModalAsk}
        footer={null}
        width={800}
        closable={true}
        onCancel={handleChange}
        title="Hãy đặt ra câu hỏi của bạn"
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Row className="content">
            <Col className="selection" span={24}>
              <Form.Item
                name="subject"
                rules={[{ required: true, message: "Subject is required" }]}
              >
                <Select
                  mode="multiple"
                  placeholder="Please select"
                  className="selection-op"
                >
                  {option.map((data, key) => {
                    return (
                      <Option key={key} value={data.value}>
                        {data.label}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
              <Form.Item
                name="point"
                label="Point"
                rules={[{ required: true, message: "Point is required" }]}
              >
                <Select defaultValue="" className="selection-option">
                  <Option value="10">10</Option>
                  <Option value="20">20</Option>
                  <Option value="30">30</Option>
                  <Option value="40">40</Option>
                  <Option value="50">50</Option>
                </Select>
              </Form.Item>
              <div>
                <Switch
                  checkedChildren="Công khai"
                  unCheckedChildren="Ẩn danh"
                  checked={checked}
                  defaultChecked
                  onChange={postAnonymous}
                />
              </div>
            </Col>
            <Col class="title-content" span={24}>
              <Form.Item
                name="title"
                rules={[
                  { required: true, message: "Title is required" },
                  { type: "string", max: 50 },
                ]}
              >
                <Input placeholder="Nhập tiêu đề của câu hỏi" />
              </Form.Item>
            </Col>
            <Col className="content-post-detail">
              {/* <ReactQuill
                theme="snow"
                onChange={contentPost}
                placeholder="Controlled autosize"
              /> */}
              <MDEditor value={content} onChange={setContent} preview="edit" />
              <Col className="content-post-tuto">
                <div className="content-post-tuto-code">
                  <span>```</span>
                  <div className="tuto-code">code</div>
                  <span>```</span>
                </div>
                <div className="content-post-tuto-bold">
                  <span>**</span>
                  <div className="tuto-bold">bold</div>
                  <span>**</span>
                </div>
                <div className="content-post-tuto-italic">
                  <span>*</span>
                  <div className="tuto-italic">italic</div>
                  <span>*</span>
                </div>
                <div className="content-post-tuto-quote">
                  <span>></span>
                  <div className="tuto-quote">quote</div>
                </div>
                <div className="content-post-tuto-down-line">
                  <span>\</span>
                  <div className="tuto-down-line">
                    enter key to down the line
                  </div>
                </div>
                <div className="content-post-tuto-syntax">
                  <a
                    target="_blank"
                    href="https://www.markdownguide.org/basic-syntax/"
                  >
                    markdown syntax
                  </a>
                </div>
              </Col>
              {content == null || content == "" ? null : (
                <Col className="content-post-preview">
                  <MDEditor.Markdown source={content} />
                </Col>
              )}
            </Col>
            <Col className="upload-file" span={24}>
              <div>
                <Upload
                  // action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                  listType="picture-card"
                  // fileList={fileList}
                  onPreview={handlePreview}
                  onChange={handleChangeImage}
                >
                  <div>
                    <i class="bx bx-upload"></i>
                  </div>
                </Upload>
                <Modal
                  visible={previewVisible}
                  footer={null}
                  onCancel={handleCancelImage}
                >
                  <img alt="example" style={{ width: "100%" }} src={fileList} />
                </Modal>
              </div>
            </Col>
            <Col className="submit-post" span={24}>
              <div>Bạn hiện có {userPost?.point} điểm</div>
              <div>
                <Button type="primary" htmlType="submit" loading={loading}>
                  {loading ? "Loading..." : "Gửi câu hỏi"}
                </Button>
              </div>
            </Col>
          </Row>
        </Form>
      </Modal>
      <Comfirm
        isComfirm={isComfirm}
        setIsComfirm={setIsComfirm}
        setChecked={setChecked}
      />
    </>
  );
};

export default AskTemplate;
