const getChampionUser = (point) => {
  if (point > 10) {
  }
};

const getSubject = (id) => {
  if (id == 1) {
    return "Java";
  } else if (id == 2) {
    return "PHP";
  } else if (id == 3) {
    return "C++";
  } else if (id == 4) {
    return "JavaScript";
  } else if (id == 5) {
    return "Database";
  }
};

const initReasonList = [
  { id: 1, content: "Câu hỏi không đầy đủ, thiếu logic, không liên quan" },
  { id: 2, content: "Spam" },
  { id: 3, content: "Câu hỏi chứa thông tin cá nhân" },
  { id: 4, content: "Chứa thông tin cá nhân" },
  { id: 5, content: "Vi phạm điều khoản" },
  { id: 6, content: "Câu hỏi chứa nội dung thô tục, kích động, bạo lực" },
  { id: 7, content: "Khác..." },
];

const listRank = [
  {
    id: 1,
    title: "Lính mới",
    point: 0,
    content:
      "Chào mừng bạn đến với Cộng đồng Fpoly. Hãy bắt đầu bằng việc trả lời các câu hỏi để tăng điểm và thăng hạng nhé!",
    img: "./images/rank.png",
  },
  {
    id: 2,
    title: "Quân tâm",
    point: 100,
    content:
      "Bạn đã dần làm quen với những điều thú vị ở đây và muốn giúp đỡ thêm nhiều người bằng những câu trả lời hữu ích của mình rồi chứ? Cố gắng lên nào!",
    img: "./images/logo-rank2.png",
  },
  {
    id: 3,
    title: "Tích cực",
    point: 250,
    content:
      "Mọi người đều thấy bạn là một thành viên tích cực. Hãy chăm chỉ và nỗ lực hơn nữa để có những câu trả lời hay hơn!",
    img: "./images/logo-rank3.png",
  },
  {
    id: 4,
    title: "Hiểu biết",
    point: 500,
    content:
      "Bạn thực sự là một thành viên nổi bật của Cộng đồng. Chỉ có những người có kiến thức tốt và được ghi nhận như bạn mới có thể được gọi là hiểu biết.",
    img: "./images/logo-rank4.png",
  },
  {
    id: 5,
    title: "Tài năng",
    point: 1000,
    content:
      "Bạn có khả năng xuất sắc trong những môn học thế mạnh của mình và được nhiều thành viên đánh giá cao với những câu trả lời không chỉ chính xác mà còn thú vị nữa.",
    img: "./images/logo-rank5.png",
  },
  {
    id: 6,
    title: "Thông thái",
    point: 3000,
    content:
      "Không chỉ đơn thuần tài năng, bạn còn thực sự là một “chuyên gia” không thể thiếu của Cộng đồng. Một chút cố gắng nữa để có thể đạt tới hạng cao nhất nào!",
    img: "./images/logo-rank6.png",
  },
  {
    id: 7,
    title: "Thiên tài",
    point: 15000,
    content:
      "Cộng đồng Fpoly cần bạn! Trí tuệ, sự cống hiến và thái độ tích cực của bạn xứng đáng được tôn trọng. Cho đi là nhận lại, những điều bạn chia sẻ đã giúp đỡ rất nhiều người. Đừng từ bỏ thói quen tốt đẹp đó để cộng đồng của chúng ta ngày càng phát triển hơn nữa nhé!",
    img: "./images/logo-rank7.png",
  },
];

const getUserRank = (point) => {
  let result = {};
  if (point >= 0 && point < 100) {
    result = {
      title: "Lính mới",
      img: "../images/rank.png",
    };
  } else if (point >= 100 && point < 250) {
    result = {
      title: "Quân tâm",
      img: "../images/logo-rank2.png",
    };
  } else if (point >= 250 && point < 500) {
    result = {
      title: "Tích cực",
      img: "../images/logo-rank3.png",
    };
  } else if (point >= 500 && point < 1000) {
    result = {
      title: "Hiểu biết",
      img: "../images/logo-rank4.png",
    };
  } else if (point >= 1000 && point < 3000) {
    result = {
      title: "Tài năng",
      img: "../images/logo-rank5.png",
    };
  } else if (point >= 3000 && point < 15000) {
    result = {
      title: "Thông thái",
      img: "../images/logo-rank6.png",
    };
  } else if (point >= 15000) {
    result = {
      title: "Thiên tài",
      img: "../images/logo-rank7.png",
    };
  }
  return result;
};

const getEuclid = [
  { id: 1, name: "FPT Polytechnic Hà Nội" },
  { id: 2, name: "FPT Polytechnic Đà Nẵng" },
  { id: 3, name: "FPT Polytechnic Hồ Chí Minh" },
  { id: 4, name: "FPT Polytechnic Tây Nguyên" },
  { id: 5, name: "FPT Polytechnic Cần Thơ" },
  { id: 6, name: "FPT Polytechnic HiTech" },
  { id: 7, name: "FPT PTCĐ Hà Nội" },
  { id: 8, name: "FPT PTCĐ Hồ Chí Minh" },
  { id: 9, name: "FPT PTCĐ Đà Nẵng" },
  { id: 10, name: "FPT PTCĐ Cần Thơ" },
  { id: 11, name: "FPT PTCĐ Tây Nguyên" },
  { id: 12, name: "FPT PTCĐ Hải Phòng" },
  { id: 12, name: "FPT PTCĐ Huế" },
  { id: 13, name: "FPT PTCĐ Đồng Nai" },
  { id: 14, name: "FPT PTCĐ Bắc Giang" },
]; 

export {
  getChampionUser,
  getSubject,
  initReasonList,
  listRank,
  getUserRank,
  getEuclid,
};
