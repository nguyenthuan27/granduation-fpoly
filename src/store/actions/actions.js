import {
  SEARCH_INPUT_CHANGE,
  FETCH_USER_SUCCESS,
} from "../constants/constants";
import postApi from '../../api/post';

export const searchChangeAction = (searchValue) => {
  return (dispatch) => {
    dispatch({
      type: SEARCH_INPUT_CHANGE,
      payload: searchValue,
    });
  };
};

export const fetchUsersAction = async (data) => {
  return (dispatch) => {
    dispatch({
      type: FETCH_USER_SUCCESS,
      payload: data,
    });
  };
};
